<?php
use Roots\Sage\Extras;
get_template_part('templates/page', 'header');

$ID = 51;
$content = Extras\get_content($ID);
$lowerContent = get_field( 'lower_copy', $ID );
$lowerTitle = get_field( 'lower_title', $ID );

if ($content !== "") :
?>
  <div class="careers-intro">
    <?= $content; ?>
  </div>
<?php endif; ?>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
<div class="row">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
  <?php endwhile; ?>
</div>
<?php if($lowerContent !=="") : ?>
  <div class="lower-copy">
    <?php if($lowerTitle) : ?>
      <h2><?= $lowerTitle; ?></h2>
    <?php endif; ?>
    <?= $lowerContent; ?>
  </div>
<?php endif; ?>
<?php the_posts_navigation(); ?>
