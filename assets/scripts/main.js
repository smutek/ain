/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {


  /***** Equal Height Function *****/
  var equalheight = function (container) {

    var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = [],
      $el,
      $currentDiv,
      topPosition = 0;
    $(container).each(function () {

      $el = $(this);
      $($el).height('auto');
      topPostion = $el.position().top;

      if (currentRowStart !== topPostion) {
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
          rowDivs[currentDiv].height(currentTallest);
        }
        rowDivs.length = 0; // empty the array
        currentRowStart = topPostion;
        currentTallest = $el.height();
        rowDivs.push($el);
      } else {
        rowDivs.push($el);
        currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
      for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
    });
  };

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function () {
        // JavaScript to be fired on all pages

        // For project thumbnails, touch once to trigger hover state,
        // touch again to trigger click.

        var $bind = 'touchstart';

        $('.module-home-projects').bind($bind, function () {
        });

        $('.thumbnail-image').bind($bind, function () {
        });

        // fitvids
        $([".project-video", ".video-wrap"]).fitVids();

        /**
         * Vertically center Bootstrap 3 modals so they aren't always stuck at the top
         * credit: https://gist.github.com/claviska/ab56d08bb19d011e3c35
         */
        $(function () {
          function reposition() {
            var modal = $(this),
              dialog = modal.find('.modal-dialog');
            modal.css('display', 'block');

            // Dividing by two centers the modal exactly, but dividing by three
            // or four works better for larger screens.
            dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
          }

          // Reposition when a modal is shown
          $('.modal').on('show.bs.modal', reposition);
          // Reposition when the window is resized
          $(window).on('resize', function () {
            $('.modal:visible').each(reposition);
          });
        });

        // remove collapse class from toggle button
        // for some reason BS is not handling this for us
        // we need this to turn the icon bars into an X
        $('.navbar-toggle').click(function () {
          $(this).toggleClass('collapsed');
          $(this).toggleClass('open');
        });

        /*
         * Sticky Nav - courtesy of Headroom
         * CSS animations via animate.css
         *
         * The nav animations need not run on screen sizes above 992
         * (the point where the nav becomes uncollapsed) so the
         * init and destroy calls for Headroom are wrapped in another
         * function that checks the page width on load, and on window
         * resize.
         *
         * @see https://github.com/WickyNilliams/headroom.js
         * @see http://stackoverflow.com/a/9720333 (check width function)
         */

        // Setup Headroom
        var navbar = document.querySelector('header.navbar');
        var headroom = new Headroom(navbar, {
          "offset": 205,
          "tolerance": {
            "up": 20,
            "down": 5
          },
          "classes": {
            "initial": "animated",
            "pinned": "slideInDown",
            "unpinned": "slideOutUp"
          }
        });

        //jQuery to collapse the navbar on scroll - changed from > 25 to 110 to not show white background
        $(window).scroll(function () {
          if ($(".navbar").offset().top > 110) {
            $(".navbar-fixed-top").addClass("top-nav-collapse");
            $("main").addClass("top-nav-collapse");
          } else {
            $(".navbar-fixed-top").removeClass("top-nav-collapse");
            $("main").removeClass("top-nav-collapse");
          }
        });

        // check page width. Get the current page width.
        // if it is less than 992 init headroom
        // if it is greater than 992 destroy headroom
        var $window = $(window),
          $collapsePoint = 992;

        function checkWidth() {
          var windowsize = $window.width();
          if (windowsize > $collapsePoint) {
            headroom.destroy();
          } else {
            headroom.init();
          }
        }

        // Execute on load
        checkWidth();
        // Bind event listener
        $(window).resize(checkWidth);

        // Items to run the equal height function on
        var $equalHeightArray = [
          '.equal-height .card',
          '.contact-wrap .col-md-6',
          '.client-logo .thumbnail-image',
          '.img-text-block .image-container'
        ];
        // Iterate through the array, if the element is found run the function on
        // window load and on window resize.
        $.each($equalHeightArray, function(index, value){
          if(value) {
            $(window).load(function () {
              var $window = $(window).width();
              if ($window >= 768) {
                equalheight(value);
              }
              $(window).resize(function () {
                equalheight(value);
              });
            });
          }
        });

        // page loader
        window.addEventListener("load", function (event) {
          $('.loading').fadeOut("slow");
        });
      },
      finalize: function () {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    'infinite_load': {
      init: function() {

        $('.main .card-outer').removeClass('post-loading');

        // Load more posts
        var buttonMarkup = '<div class="load-more"><button type="button">Load More</button></div>',
          finishedMarkup = '<div class="no-more disabled"><button type="button" disabled="disabled">All Posts Loaded</button></div>',
          container = '.main',
          target = '.load-more',
          button = $(container + ' ' + target),
          page = 1,
          loading = false,
          done = false,
          // the value to offset the query by
          offsetBy = parseInt(ajaxLoadMore.offsetBy),
          // posts per page value
          postsPerPage = parseInt(ajaxLoadMore.postsPerPage),
          totalPosts = parseInt(ajaxLoadMore.totalPosts),
          // How many posts we're starting with. Values are different between News & Insights
          startingNumber = parseInt(ajaxLoadMore.startingNumber),
          // How many posts are left to load
          remainingPosts = totalPosts - startingNumber;

        $(container).append( buttonMarkup );
        $(target).fadeIn();

        $('body').on('click', '.load-more', function(){
          // if not currently loading....
          if( ! loading ) {
            // we are now loading!
            loading = true;
            $(target).fadeOut('slow');
            // Decrement the remaining posts
            remainingPosts = remainingPosts - postsPerPage;
            // If the remaining posts is 0 or less, set done to true
            if(remainingPosts <=  0) {
              done = true;
              remainingPosts = 0;
            }
            // This is the data that gets passed into the HTTP Request (the ajax call).
            // It's what we want PHP to give us at this point.
            var data = {
              // the action we want to fire, corresponds to our PHP function / query
              action: 'ajaxLoadMore',
              // get the current page from PHP
              page: page,
              // calculate the offset to pass into the query
              offset: (postsPerPage * page) + offsetBy,
              // the query to run
              query: ajaxLoadMore.query
            };
            // Load data from the server using an HTTP post request
            // see: https://api.jquery.com/jquery.post/
            // jQuery.post( url [, data ] [, success ] [, dataType ] )
            //
            // Send our query to the server, and if the request succeeds...
            $.post(ajaxLoadMore.url, data, function(res) {
              if( res.success) {

                // get the height of the parent container
                var oldHeight = $(container).height();

                // add the new posts to the bottom of the container
                $(container).append( res.data );

                // See if there are more posts
                if(done) {
                  // If we are done, IE. remaining posts === 0
                  // Remove the load more button
                  $(target).remove();
                  // Add the finished button
                  $(container).append( finishedMarkup );
                  // Change the value of the button target var, since
                  // it's added back in after animations complete
                  target = '.main .no-more';
                } else {
                  // otherwise, move the load more button below the posts
                  $(target).remove();
                  $(container).append(buttonMarkup);
                }

                // Eliminate visual JANK caused by the footer quickly being pushed
                // down to make room for the new content.
                // We already stored the height of the parent before the new items
                // were inserted, now get the height of the container after the new
                // items have been inserted.
                var newHeight = $(container).height();
                // Reset the parent to the old height
                $(container).height(oldHeight);
                // Now set it back to the new height with an animation.
                $(container).animate({height: newHeight}, 'slow', function() {
                  //  set the container height to auto
                  $(container).height('auto');
                  // finally, scroll the new posts
                  $('html,body').animate({scrollTop: $('.new-posts').offset().top - 200 }, 1000);
                  $(target).fadeIn();
                });
                // animate.css is being used to transition the new posts container into
                // the DOM. Run some clean up once the animations have completed.
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $('.new-posts').one(animationEnd, function() {
                  // Remove the .new-posts div that wraps around the new posts as
                  // it's just a utility being used to animate the new content
                  $('.post-loading').unwrap();
                  // remove the post-loading class so that we can safely run all this
                  // stuff on the next go-round
                  $('.main .card-outer').removeClass('post-loading');
                  // run equal height to clean up  newly inserted posts
                  equalheight('.equal-height .card');
                });
                // we are no longer loading
                loading = false;
                // increment the page number
                page = page + 1;
              }
              // end if success
            });
          }
        });
      }
    },
    'single_careers': {
      init: function() {
        // Tool tips for team member thumbs
        $('[data-toggle="tooltip"]').tooltip();

        // file upload
        $(".upload input.medium").change(function () {

          var label = this.value;
          var id = $(this).attr("id");
          var target = $("label[for=" + id + "]");
          var truncate = function (fullStr, strLen, seperator) {

            if (fullStr.length <= strLen) {
              return fullStr;
            }

            seperator = seperator || '... ';

            var sepLen = seperator.length,
              charsToShow = strLen - sepLen,
              frontChars = Math.ceil(charsToShow - 4),
              backChars = Math.floor(4);

            return fullStr.substr(0, frontChars) +
              seperator +
              fullStr.substr(fullStr.length - backChars);
          };

          $(target).html(truncate(label, 20));

          $(this).parent(target).addClass("success");
        });

      }
    },
    // Home page
    'home': {
      init: function () {
        // JavaScript to be fired on the home page
      },
      finalize: function () {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about': {
      init: function () {
        // JavaScript to be fired on the about us page
        // init Masonry
        var $grid = $('.gallery').masonry({
          itemSelector: '.gallery-image',
          columnWidth: '.gallery-sizer',
          percentPosition: true
        });
// layout Masonry after each image loads
        $grid.imagesLoaded().progress(function () {
          $grid.masonry('layout');
        });
      }
    },

    'has_video_modal': {
      init: function () {

        // init media element -
        // Controls -
        // @see https://github.com/johndyer/mediaelement/blob/master/api.md
        // Nice example of hooking into events -
        // @see https://webdevstudios.com/2014/10/17/customizing-mediaelement-js-for-wordpress/

        var video = new MediaElementPlayer( '.modal-video', {
          // set dimensions via JS instead of CSS
          setDimensions: true,
          // stretching modes (auto, fill, responsive, none)
          stretching: 'responsive'
        });

        // stop video playback on modal close

        /*
         ================ Auto Play Video on Modal Launch ================

         notes

         -- Update 12/27 - we may want to review this function since
          we are using mediaelement.js now, which offers its own events to hook onto.
          I'm leaving it for now since the events we are watchign for are bootstrap
          modal states.

         -- Update 12/26 - The logic here has slimmed down considerably since we switched to
           using the native HTML5 media player over Vimeo's hosted player. I'm leaving the
           previous comments here in case we need to switch back.

         -- when setting auto play to true Firefox will wait to auto play the video until the
         modal has been triggered, which is nice, but Chrome and Safari autoplay the video immediately,
         which is annoying.

         This code fixes this by storing 2 variables upon page load. One is the video's unaltered URL
         and the second is the URL with the autoplay=1 query string appended.

         We listen for 3 events -

         1. When the modal is triggered but before it has started loading (show.bs.modal event)
         - the 'video-modal-open' class is added to the body, to change the backdrop color.
         (this needs to be swapped in like this because there are other modals on the page that use
         a white backdrop)

         2. When the modal is loaded and animations are completed (shown.bs.modal event)
         - the auto play URL is swapped in and video plays.

         3. When the modal is closed (hidden.bs.modal event)
         - the original URL, sans query string, is restored. Modal closes, video stops playing, everybody happy.

         Doing it this way makes for a much smoother animation, chaining the SRC swap to the show event created a
         noticeable jerk in the animation, but chaining the css chain to the shown event caused a white flicker.
          Splitting them up makes everything nice and dandy.

         @see http://getbootstrap.com/javascript/#modals-events

         The problem with this implementation is that it only supports one video per page. I was wracking my brain
         trying to find a solution that will work for unique elements but it's late and I'm on vacation, so this will
         do for now.

         If this comes back up and we need to display multiple video modals on a page then I think the best approach would
         be to grab the ID's and URL's of each video and store them in an array as key value pairs [$ID => $URL] then do a
         match when an event is fired.  - JS
         */

        // set vars
        var videoModal = ".video-modal",
          target = ".video-modal video";
        // when modal is triggered...
        $(videoModal).on('show.bs.modal', function (e) {
          $('body').addClass('video-modal-open');
        });
        // when modal is triggered...
        $(videoModal).on('shown.bs.modal', function (e) {
          // start playback
          $(target)[0].play();
        });
        // when modal is closed
        $(videoModal).on('hidden.bs.modal', function (e) {
          // stop playback
          $(target)[0].pause();
          $('body').removeClass('video-modal-open');
        });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
