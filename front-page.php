<?php
while (have_posts()) : the_post();
  get_template_part('templates/modules/module', 'video-hero');
  get_template_part('templates/modules/module', 'home-mission');
  get_template_part('templates/modules/module', 'home-projects');
  get_template_part('templates/modules/module', 'home-about');
  get_template_part('templates/modules/module', 'home-culture');
  get_template_part('templates/modules/module', 'home-insights');
  get_template_part('templates/modules/module', 'contact-cta');
endwhile;
