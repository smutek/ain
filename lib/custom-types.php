<?php
/**
 * Custom Types
 *
 * Determines the custom post types used in the theme.
 * Add or remove files to the array as needed.
 * Based on the Sage Includes array in functions.php
 *
 * Please note that missing files will produce a fatal error.
 */
$customTypes = [
  'lib/custom-types/news-cpt.php', // News CPT
  'lib/custom-types/team-cpt.php', // Team CPT
  'lib/custom-types/jobs-cpt.php', // Jobs CPT
  'lib/custom-types/projects-cpt.php', // Projects CPT
  'lib/custom-types/clients-cpt.php', // Clients CPT
  'lib/custom-types/testimonials-cpt.php', // Testimonials CPT
  'lib/custom-types/teams-taxonomy.php', // Teams taxonomy
  'lib/custom-types/projects-taxonomy.php' // Projects taxonomy
];

foreach ($customTypes as $customType) {
  if (!$filepath = locate_template($customType)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $customType), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
