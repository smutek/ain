<?php

// Register Custom Post Type
function jobsCPT() {

  $labels = array(
    'name'                  => _x( 'Jobs', 'Post Type General Name', 'sage' ),
    'singular_name'         => _x( 'Job', 'Post Type Singular Name', 'sage' ),
    'menu_name'             => __( 'Jobs', 'sage' ),
    'name_admin_bar'        => __( 'Job', 'sage' ),
    'archives'              => __( 'Item Archives', 'sage' ),
    'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
    'all_items'             => __( 'All Jobs', 'sage' ),
    'add_new_item'          => __( 'Add Job', 'sage' ),
    'add_new'               => __( 'Add Job', 'sage' ),
    'new_item'              => __( 'New Job', 'sage' ),
    'edit_item'             => __( 'Edit Job', 'sage' ),
    'update_item'           => __( 'Update Job', 'sage' ),
    'view_item'             => __( 'View Job', 'sage' ),
    'search_items'          => __( 'Search Item', 'sage' ),
    'not_found'             => __( 'Not found', 'sage' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
    'insert_into_item'      => __( 'Insert into item', 'sage' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
    'items_list'            => __( 'Items list', 'sage' ),
    'items_list_navigation' => __( 'Items list navigation', 'sage' ),
    'filter_items_list'     => __( 'Filter items list', 'sage' ),
  );
  $rewrite = array(
    'slug'                  => 'careers',
    'with_front'            => false,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __( 'Jobs', 'sage' ),
    'description'           => __( 'CPT for jobs', 'sage' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'author', 'revisions', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-id-alt',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => $rewrite,
    'capability_type'       => 'page',
  );
  register_post_type( 'careers', $args );

}
add_action( 'init', 'jobsCPT', 0 );
