<?php

// Register Custom Post Type
function newsCPT() {

  $labels = array(
    'name'                  => _x( 'News', 'Post Type General Name', 'sage' ),
    'singular_name'         => _x( 'News Post', 'Post Type Singular Name', 'sage' ),
    'menu_name'             => __( 'News', 'sage' ),
    'name_admin_bar'        => __( 'News Post', 'sage' ),
    'archives'              => __( 'Item Archives', 'sage' ),
    'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
    'all_items'             => __( 'All News Posts', 'sage' ),
    'add_new_item'          => __( 'Add News Post', 'sage' ),
    'add_new'               => __( 'Add News Post', 'sage' ),
    'new_item'              => __( 'New News Post', 'sage' ),
    'edit_item'             => __( 'Edit News Post', 'sage' ),
    'update_item'           => __( 'Update News Post', 'sage' ),
    'view_item'             => __( 'View News Post', 'sage' ),
    'search_items'          => __( 'Search Item', 'sage' ),
    'not_found'             => __( 'Not found', 'sage' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
    'insert_into_item'      => __( 'Insert into item', 'sage' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
    'items_list'            => __( 'Items list', 'sage' ),
    'items_list_navigation' => __( 'Items list navigation', 'sage' ),
    'filter_items_list'     => __( 'Filter items list', 'sage' ),
  );
  $rewrite = array(
    'slug'                  => 'news',
    'with_front'            => false,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __( 'News Posts', 'sage' ),
    'description'           => __( 'CPT for News', 'sage' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-megaphone',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'rewrite'               => $rewrite
  );
  register_post_type( 'news', $args );

}
add_action( 'init', 'newsCPT', 0 );
