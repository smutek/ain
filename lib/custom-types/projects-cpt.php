<?php

// Register Custom Post Type
function projectsCPT() {

  $labels = array(
    'name'                  => _x( 'Projects', 'Post Type General Name', 'sage' ),
    'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'sage' ),
    'menu_name'             => __( 'Projects', 'sage' ),
    'name_admin_bar'        => __( 'Project', 'sage' ),
    'archives'              => __( 'Item Archives', 'sage' ),
    'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
    'all_items'             => __( 'All Projects', 'sage' ),
    'add_new_item'          => __( 'Add Project', 'sage' ),
    'add_new'               => __( 'Add Project', 'sage' ),
    'new_item'              => __( 'New Project', 'sage' ),
    'edit_item'             => __( 'Edit Project', 'sage' ),
    'update_item'           => __( 'Update Project', 'sage' ),
    'view_item'             => __( 'View Project', 'sage' ),
    'search_items'          => __( 'Search Item', 'sage' ),
    'not_found'             => __( 'Not found', 'sage' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
    'insert_into_item'      => __( 'Insert into item', 'sage' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
    'items_list'            => __( 'Items list', 'sage' ),
    'items_list_navigation' => __( 'Items list navigation', 'sage' ),
    'filter_items_list'     => __( 'Filter items list', 'sage' ),
  );
  $rewrite = array(
    'slug'                  => 'work',
    'with_front'            => false,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __( 'Projects', 'sage' ),
    'description'           => __( 'CPT for projects', 'sage' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'author', 'revisions', ),
    'taxonomies'            => array( 'project-tags' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-welcome-view-site',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => $rewrite,
    'capability_type'       => 'page',
  );
  register_post_type( 'projects', $args );

}
add_action( 'init', 'projectsCPT', 0 );
