<?php
// Register Custom Taxonomy

function projectsTaxonomy() {

  $labels = array(
    'name'                       => _x( 'Project Tags', 'Taxonomy General Name', 'sage' ),
    'singular_name'              => _x( 'Project Tag', 'Taxonomy Singular Name', 'sage' ),
    'menu_name'                  => __( 'Project Tags', 'sage' ),
    'all_items'                  => __( 'All Project Tags', 'sage' ),
    'parent_item'                => __( 'Parent Item', 'sage' ),
    'parent_item_colon'          => __( 'Parent Item:', 'sage' ),
    'new_item_name'              => __( 'New Project Tag', 'sage' ),
    'add_new_item'               => __( 'Add New Project Tag', 'sage' ),
    'edit_item'                  => __( 'Edit Project Tag', 'sage' ),
    'update_item'                => __( 'Update Project Tag', 'sage' ),
    'view_item'                  => __( 'View Project Tag', 'sage' ),
    'separate_items_with_commas' => __( 'Separate project tags with commas', 'sage' ),
    'add_or_remove_items'        => __( 'Add or remove project tags', 'sage' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'sage' ),
    'popular_items'              => __( 'Popular Project Tags', 'sage' ),
    'search_items'               => __( 'Search Project Tags', 'sage' ),
    'not_found'                  => __( 'Not Found', 'sage' ),
    'no_terms'                   => __( 'No project tags', 'sage' ),
    'items_list'                 => __( 'Project tags list', 'sage' ),
    'items_list_navigation'      => __( 'Project tags list navigation', 'sage' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => false,
    'show_tagcloud'              => false,
  );
  register_taxonomy( 'project-tags', array( 'projects' ), $args );

}
add_action( 'init', 'projectsTaxonomy', 0 );
