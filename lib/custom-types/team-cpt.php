<?php

// Register Custom Post Type
function teamCPT() {

  $labels = array(
    'name'                  => _x( 'Team Members', 'Post Type General Name', 'sage' ),
    'singular_name'         => _x( 'Team Member', 'Post Type Singular Name', 'sage' ),
    'menu_name'             => __( 'Team', 'sage' ),
    'name_admin_bar'        => __( 'Team', 'sage' ),
    'archives'              => __( 'Item Archives', 'sage' ),
    'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
    'all_items'             => __( 'All Team Members', 'sage' ),
    'add_new_item'          => __( 'Add Team Member', 'sage' ),
    'add_new'               => __( 'Add Team Member', 'sage' ),
    'new_item'              => __( 'New Team member', 'sage' ),
    'edit_item'             => __( 'Edit Team Member', 'sage' ),
    'update_item'           => __( 'Update Team Member', 'sage' ),
    'view_item'             => __( 'View Team Member', 'sage' ),
    'search_items'          => __( 'Search Item', 'sage' ),
    'not_found'             => __( 'Not found', 'sage' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
    'featured_image'        => __( 'Head Shot', 'sage' ),
    'set_featured_image'    => __( 'Set Head Shot', 'sage' ),
    'remove_featured_image' => __( 'Remove Head Shot', 'sage' ),
    'use_featured_image'    => __( 'Use as Head Shot', 'sage' ),
    'insert_into_item'      => __( 'Insert into item', 'sage' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
    'items_list'            => __( 'Items list', 'sage' ),
    'items_list_navigation' => __( 'Items list navigation', 'sage' ),
    'filter_items_list'     => __( 'Filter items list', 'sage' ),
  );
  $rewrite = array(
    'slug'                  => 'team',
    'with_front'            => false,
    'pages'                 => true,
    'feeds'                 => true,
  );
  $args = array(
    'label'                 => __( 'Team Member', 'sage' ),
    'description'           => __( 'CPT for team', 'sage' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', ),
    'taxonomies'            => array( 'roles' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-nametag',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => $rewrite,
    'capability_type'       => 'page',
  );
  register_post_type( 'team', $args );

}
add_action( 'init', 'teamCPT', 0 );
