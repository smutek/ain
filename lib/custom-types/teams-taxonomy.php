<?php
// Register Custom Taxonomy

function rolesTaxonomy() {

  $labels = array(
    'name'                       => _x( 'Teams', 'Taxonomy General Name', 'sage' ),
    'singular_name'              => _x( 'Team', 'Taxonomy Singular Name', 'sage' ),
    'menu_name'                  => __( 'Teams', 'sage' ),
    'all_items'                  => __( 'All Teams', 'sage' ),
    'parent_item'                => __( 'Parent Item', 'sage' ),
    'parent_item_colon'          => __( 'Parent Item:', 'sage' ),
    'new_item_name'              => __( 'New Team', 'sage' ),
    'add_new_item'               => __( 'Add New Team', 'sage' ),
    'edit_item'                  => __( 'Edit Team', 'sage' ),
    'update_item'                => __( 'Update Team', 'sage' ),
    'view_item'                  => __( 'View Team', 'sage' ),
    'separate_items_with_commas' => __( 'Separate teams with commas', 'sage' ),
    'add_or_remove_items'        => __( 'Add or remove teams', 'sage' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'sage' ),
    'popular_items'              => __( 'Popular Items', 'sage' ),
    'search_items'               => __( 'Search Items', 'sage' ),
    'not_found'                  => __( 'Not Found', 'sage' ),
    'no_terms'                   => __( 'No items', 'sage' ),
    'items_list'                 => __( 'Items list', 'sage' ),
    'items_list_navigation'      => __( 'Items list navigation', 'sage' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => false,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_menu'               => true,
    'show_in_nav_menus'          => false,
    'show_tagcloud'              => false,
    'meta_box_cb'                => false
  );
  register_taxonomy( 'teams', array( 'team' ), $args );

}
add_action( 'init', 'rolesTaxonomy', 0 );
