<?php
// Register Custom Post Type
function testimonialsCPT() {

  $labels = array(
    'name'                  => _x( 'Testimonials', 'Post Type General Name', 'sage' ),
    'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'sage' ),
    'menu_name'             => __( 'Testimonials', 'sage' ),
    'name_admin_bar'        => __( 'Testimonial', 'sage' ),
    'archives'              => __( 'Item Archives', 'sage' ),
    'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
    'all_items'             => __( 'All Items', 'sage' ),
    'add_new_item'          => __( 'Add Testimonial', 'sage' ),
    'add_new'               => __( 'Add Testimonail', 'sage' ),
    'new_item'              => __( 'New Testimonial', 'sage' ),
    'edit_item'             => __( 'Edit Testimonial', 'sage' ),
    'update_item'           => __( 'Update Testimonial', 'sage' ),
    'view_item'             => __( 'View testimonial', 'sage' ),
    'search_items'          => __( 'Search Item', 'sage' ),
    'not_found'             => __( 'Not found', 'sage' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
    'featured_image'        => __( 'Client Logo', 'sage' ),
    'set_featured_image'    => __( 'Set Client Logo', 'sage' ),
    'remove_featured_image' => __( 'Remove Client Logo', 'sage' ),
    'use_featured_image'    => __( 'Use as Client Logo', 'sage' ),
    'insert_into_item'      => __( 'Insert into item', 'sage' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
    'items_list'            => __( 'Items list', 'sage' ),
    'items_list_navigation' => __( 'Items list navigation', 'sage' ),
    'filter_items_list'     => __( 'Filter items list', 'sage' ),
  );
  $args = array(
    'label'                 => __( 'Testimonial', 'sage' ),
    'description'           => __( 'CPT for testimonials', 'sage' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'author', 'revisions', ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-smiley',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => false,
    'rewrite'               => false,
    'capability_type'       => 'page',
  );
  register_post_type( 'testimonials', $args );

}
add_action( 'init', 'testimonialsCPT', 0 );
