<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register( $wp_customize ) {
  $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
}

add_action( 'customize_register', __NAMESPACE__ . '\\customize_register' );

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script( 'sage/customizer', Assets\asset_path( 'scripts/customizer.js' ), [ 'customize-preview' ], null, true );
}

add_action( 'customize_preview_init', __NAMESPACE__ . '\\customize_preview_js' );

function ainsleyCustomize( $wp_customize ) {

  // site logo
  $wp_customize->add_setting( 'secondary_logo', [
    'sanitize_callback' => 'esc_url'
  ] );
  $wp_customize->add_control(
    new\WP_Customize_Image_Control(
      $wp_customize,
      'secondary_logo_control', [
        'label'       => __( 'Secondary Logo', 'sage' ),
        'section'     => 'title_tagline',
        'description' => sprintf( __( 'Secondary logo, displays in the footer.', 'sage' ) ),
        'settings'    => 'secondary_logo',
        'priority'    => 700,
      ]
    )
  );
  /*
   * ****** Site Info Section *******
   */
  $wp_customize->add_section( 'site_info', [
    'priority'    => 10,
    'capability'  => 'edit_theme_options',
    'title'       => __( 'Site Info', 'sage' ),
    'description' => sprintf( __( 'Site info, such as phone, fax, address, etc.', 'sage' ) )
  ] );
  /*
   * *********** Company name
   */
  $wp_customize->add_setting( 'business_name', [
    'default'           => '',
    'sanitize_callback' => 'sanitize_text_field'
  ] );
  $wp_customize->add_control( 'business_name_control', [
    'label'    => __( 'Business Name', 'sage' ),
    'section'  => 'site_info',
    'settings' => 'business_name',
    'type'     => 'text'
  ] );
  /*
   * *********** Phone
   */
  $wp_customize->add_setting( 'phone_setting', [
    'default'           => '',
    'sanitize_callback' => 'sanitize_text_field'
  ] );
  $wp_customize->add_control( 'phone_control', [
    'label'    => __( 'Phone Number', 'sage' ),
    'section'  => 'site_info',
    'settings' => 'phone_setting',
    'type'     => 'text'
  ] );

  /*
  * *********** Fax
  */
  $wp_customize->add_setting( 'fax_setting', [
    'default'           => '',
    'sanitize_callback' => 'sanitize_text_field'
  ] );
  $wp_customize->add_control( 'fax_control', [
    'label'    => __( 'Fax Number', 'sage' ),
    'section'  => 'site_info',
    'settings' => 'fax_setting',
    'type'     => 'text'
  ] );

  /*
  * *********** Email
  */
  $wp_customize->add_setting( 'email_setting', [
    'default'           => '',
    'sanitize_callback' => 'sanitize_text_field'
  ] );
  $wp_customize->add_control( 'email_control', [
    'label'    => __( 'Site Email', 'sage' ),
    'section'  => 'site_info',
    'settings' => 'email_setting',
    'type'     => 'text'
  ] );
  /*
  ************ Address
  */
  $wp_customize->add_setting( 'address_line_1', [
    'default'           => '',
    'sanitize_callback' => 'sanitize_text_field'
  ] );
  $wp_customize->add_control( 'address_1_control', [
    'label'    => __( 'Address Line 1', 'sage' ),
    'section'  => 'site_info',
    'settings' => 'address_line_1',
    'type'     => 'text'
  ] );

  $wp_customize->add_setting( 'address_line_2', [
    'default'           => '',
    'sanitize_callback' => 'sanitize_text_field'
  ] );
  $wp_customize->add_control( 'address_2_control', [
    'label'    => __( 'Address Line 2', 'sage' ),
    'section'  => 'site_info',
    'settings' => 'address_line_2',
    'type'     => 'text'
  ] );

  /*
  * ********** Bits and Bobs ********
  */
  // all the things that come up that don't fit in existng panels.

  $wp_customize->add_section( 'misc_settings', [
    'priority'   => 10,
    'capability' => 'edit_theme_options',
    'title'      => 'Miscellaneous Settings'
  ] );

  // Set page for projects.
  // Simply here as a safeguard to set the ID for the projects page.
  // because individual projects call it for their previous / next
  // navigation functionality. If the ID is hard coded and the location
  // of the projects page were to change for some reason, which it never
  // would, then the previous next functionality would break on individual
  // projects. Setting an option here means that the page can be quickly
  // updated without having to involve a developer.
  $wp_customize->add_setting( 'page_for_projects_setting', array(
    'default'    => '',
    'capability' => 'edit_theme_options'
  ) );
  $wp_customize->add_control( 'page_for_projects_control', array(
    'label'       => __( 'Page for Projects', 'sage' ),
    'description' => __( 'Select the main projects page here. This setting helps the previous/next functionality.', 'sage' ),
    'section'     => 'misc_settings',
    'type'        => 'dropdown-pages',
    'settings'    => 'page_for_projects_setting'
  ) ); // end link

  // Social OG Image
  $wp_customize->add_setting( 'og_img_setting', array(
    'sanitize_callback' => 'esc_url'
  ) );
  $wp_customize->add_control(
    new \WP_Customize_Image_Control(
      $wp_customize,
      'og_img_control',
      array(
        'label'       => __( 'Open Graph Image', 'sage' ),
        'description' => __( 'Open Graph Social media image. PNG at 1200 x 630, Index color.', 'sage' ),
        'section'     => 'title_tagline',
        'settings'    => 'og_img_setting',
        'priority'    => 900
      ) )
  ); // end logo

  /*
  * *********** Company name
  */
  $wp_customize->add_setting( 'eoe_statement', [
    'default'           => '',
    'sanitize_callback' => 'sanitize_text_field'
  ] );
  $wp_customize->add_control( 'eoe_statement_control', [
    'label'    => __( 'EoE Statement', 'sage' ),
    'description'    => __( 'EoE Statement that appears below job postings.', 'sage' ),
    'section'  => 'misc_settings',
    'settings' => 'eoe_statement',
    'type'     => 'textarea'
  ] );

}

add_action( 'customize_register', __NAMESPACE__ . '\\ainsleyCustomize' );
