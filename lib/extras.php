<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class( $classes ) {
  // Add page slug if it doesn't exist
  if ( is_single() || is_page() && ! is_front_page() ) {
    if ( ! in_array( basename( get_permalink() ), $classes ) ) {
      $classes[] = basename( get_permalink() );
    }
  }

  // Add class if sidebar is active
  if ( Setup\display_sidebar() ) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}

add_filter( 'body_class', __NAMESPACE__ . '\\body_class' );

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip;';
}

add_filter( 'excerpt_more', __NAMESPACE__ . '\\excerpt_more' );

/**
 * Admin Styles
 *
 * Helps to separate ACF blocks out when there's a lot of them
 * Like on the home page
 */
function admin_styles() {
  echo '<style>
    .acf-postbox h2.hndle {
      background: #ebebeb;
      color: #424242;
      font-size: 21px !important;
    }
    .acf-postbox.closed h2.hndle {
    background: #dbdbdb;
    }
  </style>';
}

add_action( 'admin_head', __NAMESPACE__ . '\\admin_styles' );

/**
 * Site Brand
 * Use native WordPress site logo with custom (bootstrap friendly) markup
 * Falls back to text title if logo is not set.
 *
 * @return string|void
 */
function site_brand() {

  // grab the site name as set in customizer options
  $site = get_bloginfo('name');
  // if this is the home page wrap the site title in an H1, otherwise wrap in in a paragraph tag
  is_front_page() ? $output = '<h1>' . $site . '</h1>' : $output = '<p>' . $site . '</p>';
  // save the result to return to the front end
  $return = $output;
  // if there is a custom logo set in customizer options...
  if ( has_custom_logo() ) {
    // get the URL to the logo
    $logo_url = wp_get_attachment_url( get_theme_mod( 'custom_logo' ) );
    // generate some clean markup
    $logo     = '<img src="' . $logo_url . '">';
    // hide the site title by wrapping it in a screen reader friendly class
    $return   = '<span class="sr-only">' . $output  . '</span>';
    // save the site title and the logo in our return variable
    $return .= $logo;
  }
  // return the result to the front end
  return $return;
}

/**
 * Add the SVG Mime type to the uploader
 * @author Alain Schlesser (alain.schlesser@gmail.com)
 * @link https://gist.github.com/schlessera/1eed8503110fb3076e73
 *
 * @param  array $mimes list of mime types that are allowed by the
 *                        WordPress uploader
 *
 * @return array          modified version of the $mimes array
 *
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 * @see http://www.w3.org/TR/SVG/mimereg.html
 */
function as_svg_mime_type( $mimes ) {
  // add official SVG mime type definition to the array of allowed mime types
  $mimes['svg'] = 'image/svg+xml';

  // return the modified array
  return $mimes;
}

add_filter( 'upload_mimes', __NAMESPACE__ . '\\as_svg_mime_type' );

/**
 * oEmbed Attributes
 *
 * Add parameters to oEmbed query string. Useful for
 * turning off related videos and such.
 *
 * Basic field use: $video = videoLink('your_field_name');
 * Add second param if in a repeater: $video - videoLink('your_subfield_name', true);
 *
 * @see https://www.advancedcustomfields.com/resources/oembed/
 *
 * @param $field
 * @param bool $repeater defaults to false / true if repeater
 * @param bool $autoplay defaults to false. Pass true for autoplay
 *
 * @return mixed  embed HTML
 */
function videoLink( $field, $repeater = false, $autoplay = false ) {

  global $post;

  // get current post ID
  $id = $post->ID;

  if ( ! $repeater ) {
    // get the field
    $videoFrame = get_field( $field, $id );
  } else {
    // if we are in a repeater
    $videoFrame = get_sub_field( $field, $id );
  }

  // use preg_match to find iframe src
  preg_match( '/src="(.+?)"/', $videoFrame, $matches );
  $src = $matches[1];

  // add extra params to iframe src
  $params = array(
    'rel'      => 0,
    'title'    => 0,
    'byline'   => 0,
    'portrait' => 0,

    'autoplay' => $autoplay
  );

  $new_src   = add_query_arg( $params, $src );
  $videoLink = str_replace( $src, $new_src, $videoFrame );

  return $videoLink;

}

/**
 * Checks to see  if a video modal is being used.
 * Allows us to hook in JavaScript via DOM based routing,
 * and conditionally enqueue stylesheets.
 *
 * todo-jimmy Loads mediaelement on archive views for some reason.
 *
 * @return bool
 */
function hasVideoModal() {

  // if there's a pop up video, return true, else return false
  get_field( 'popup_video_url' ) ? $return = true : $return = false;

  if(is_home() || is_post_type_archive()) {
    $return = false;
  }

  return $return;
}

/**
 * Adds "video-modal" class to the body tag if a video modal is being used.
 * Allows us to hook in JavaScript via DOM based routing.
 *
 * @param $classes
 *
 * @return array
 */
function videoModalClasses($classes) {

  if(hasVideoModal()) {
    $classes[] = 'has-video-modal';
  }

  return $classes;
}
add_filter( 'body_class', __NAMESPACE__ . '\\videoModalClasses' );


/**
 * Change excerpt length to 15 words
 */
function custom_excerpt_length( $length ) {
  $return = 15;
  if ( is_page_template( 'template-home.php' ) ) {
    $return = 8;
  }

  return $return;
}

add_filter( 'excerpt_length', __NAMESPACE__ . '\\custom_excerpt_length', 999 );

/**
 * Connect user to team post
 *
 * @param $postID
 *
 * There is a "user" ACF field attached to the team post type that allows
 * a user to be selected on the back end. This function will store the ID
 * of the submitted Team post in the selected users post meta, linking the
 * team post to the corresponding user.0
 *
 * It fires when the user updates or publishes a post, but BEFORE the post
 * has been saved to the database.
 *
 * It compares the submitted value  to the existing value, and updates or
 * deletes the meta field from the corresponding user. If a user is selected
 * who has already been assigned it will erase the users previous assignment
 * and update it with the new one. This way no user can be assigned to multiple
 * team posts. This is good for now, but leaving two to-do's:
 *
 * todo-jimmy display which user the post is currently connected to
 * todo-jimmy disallow over-writing
 **/

function updateUserMeta( $postID ) {

  // get_field value - Data has not yet been saved, check what the existing value is.
  $existingUser = get_field( 'user_connect_account', $postID );
  // grab the numeric user ID from the user object
  $existingUserID = $existingUser['ID'];

  // an array of ACF fields that are being submitted
  // made available to you thanks to the ACF/save_post action
  // @see https://www.advancedcustomfields.com/resources/acfsave_post/
  $fieldsArray = $_POST['acf']; // returns an array of fields
  // Here's our field. I got this by running the debugger.
  $field = $fieldsArray['field_581cc9cffdb2c']; // our custom field
  // Change the name, to avoid confusion. This is not a time for confusion.
  $submittedUserID = $field;
  // if the submitted user ID is the same as the existing user ID, we are not updating
  $submittedUserID === $existingUserID ? $updating = false : $updating = true;
  // see if the field has a value. It can be set null by the user.
  $existingUserID === null || $existingUserID === "" ? $fieldNotSet = true : $fieldNotSet = false;

  // if we are updating we need to remove the post ID from the previous user
  if ( $updating || $fieldNotSet ) {
    // delete user meta from the previous user
    delete_user_meta( $existingUserID, 'team_id' );
  }

  // add a user meta field to the user account
  update_user_meta( $submittedUserID, 'team_id', $postID );
}

add_action( 'save_post_team', __NAMESPACE__ . '\\updateUserMeta', 10, 3 );

/**
 * Add Google Maps Key
 */
function acfInit() {
  acf_update_setting( 'google_api_key', 'AIzaSyB4c4x-CsjV5UsAcZPQaYOweIxg0Gf8BoQ' );
}

add_action( 'acf/init', __NAMESPACE__ . '\\acfInit' );

/**
 * Default Phone for contact template
 *
 * Loads the phone number from the customizer in as the default
 * phone number for the associated ACF field.
 * (IE. number is already entered in customizer as part of the
 * global site data, set it as the default for the contact page)
 *
 * @param $field
 *
 * @return mixed
 */
function loadPhone( $field ) {

  $phone = get_theme_mod( 'phone_setting' );

  $field['default_value'] = $phone;

  return $field;

}

add_filter( 'acf/load_field/name=contact_phone_number', __NAMESPACE__ . '\\loadPhone' );

/**
 * Probably a phone
 *
 * Uses Mobile_Detect class to check user agent. Checks to
 * see if the sniffed device is a mobile device, but not a tablet.
 * If it is a mobile device, but not a tablet, it's probably
 * a phone.
 *
 * Returns true or false.
 *
 * Use sparingly because user agent sniffing is sketchy.
 *
 * @return bool
 */
function probablyAPhone() {
  // initiate the class
  $detect = new \Mobile_Detect();
  // catch all. Includes phones and tablets.
  $detect->isMobile() ? $mobile = true : $mobile = false;
  $detect->isTablet() ? $tablet = true : $tablet = false;

  $mobile && ! $tablet ? $probPhone = true : $probPhone = false;

  return $probPhone;
}

/**
 * Base files for CPT's
 * Use base-$cptName.php for singles
 * Fall back to the regular stuff
 *
 * @param $templates
 *
 * @see https://roots.io/sage/docs/theme-wrapper/#filtering-the-wrapper-custom-post-types
 * @return mixed
 */
function sage_wrap_base_cpts( $templates ) {
  // Get the current post type
  $cpt = get_post_type();
  if ( $cpt ) {
    // Shift the template to the front of the array
    array_unshift( $templates, 'base-' . $cpt . '.php' );
  }

  // Return our modified array with base-$cpt.php at the front of the queue
  return $templates;
}

// Add our function to the sage/wrap_base filter
add_filter( 'sage/wrap_base', __NAMESPACE__ . '\\sage_wrap_base_cpts' );

/**
 * Offset posts per page on insights and news views
 *
 * We are using an odd number on initial page load, on insights and news,  due to the
 * large featured style image at the top of the page. Subsequent (paginated, ajax loaded)
 * pages do not feature the large image and need to be set to display an
 * even number of posts.
 *
 * This function will load an extra post on initial page load, then on
 * subsequent loads (clicks of load more) will load in the value specified in
 * settings/reading/Blog Pages Show At Most... Posts
 *
 *
 * @param $query
 *
 * @return mixed
 */
function postsOffset( $query ) {
  // if this is the blog page or the news archive
  if ( is_home() || is_post_type_archive( 'news' ) ) {
    // init a variable
    $pageNumber = 0;
    // assign the 'page' query variable to our $pageNumber variable.
    // it's wrapped in an isset to avoid kicking up PHP notices.
    // The variable evaluates to "" (empty) on first page load. After the
    // first click of the load more button it evaluates to 1. 2 on the next click,
    // etc. etc.
    if(isset($query->query_vars['page'])) {
      $pageNumber = $query->query_vars['page'];
    }
    // if $pagenumber = "" then this is the initial page load.
    if ( $pageNumber === "" ) {
      // get the user set posts per page value from wp admin and add 1 to it
      $posts = get_option( 'posts_per_page' ) + 1;
      // set this as the new posts per page value for subsequent loads (ie. load this many
      // on subsequent presses of the load more button)
      $query->set( 'posts_per_page', $posts );
    }
  }
}

add_action( 'pre_get_posts', __NAMESPACE__ . '\\postsOffset', 20 );


/**
 * Counter
 * Runs a counter from inside a loop.
 * @return int
 */
function counter() {
  static $count = 0;
  $count ++;

  return $count;
}

/**
 * Is First Post
 *
 * Checks to see if the current post is the first post.
 *
 * @return bool true/false
 */
function isFirstPost() {

  $paged = get_query_var('paged');

  $paged === 0  && counter() === 1 ? $return = true : $return = false;

  return $return;
}

/**
 * Team Titles
 *
 * Takes repeater field, and subfield as arguments, optionally takes a sep char (defaults to ", ")
 * Returns formatted string.
 *
 * This function was born because originally the titles for team members was set up as a simple text
 * field. Some team members have multiple titles. On closer inspection of the design, and mid development
 * it was revealed that the separating character differed between pages for these team members. Ie.,
 * for Tom, with the titles "CEO" and "Creative Director", on some pages this appeared as "CEO, Creative Director"
 * where on others it appeared as "CEO | Creative Director". Reacting to this I set up titles to be entered as
 * a repeater field, and wrote this function so the separating char could be passed in to the function.
 *
 * In retrospect, I now realize the differing characters were most likley an oversight on designs behalf and
 * this was for moot.
 *
 * @param $field
 * @param $subField
 * @param string $sep
 *
 * @return string
 */
function teamTitles($field, $subField, $sep=", ") {
  // Get the repeater field
  $titleField = get_field( $field );
  // Init empty array
  $array = [];
  // Loop through the repeater and add each subfield value to the array
  foreach ($titleField as $titleSubField) {
    $array[] = $titleSubField[$subField];
  }
  // clean out any empty fields that may have been left accidentally
  $clean = array_filter($array);
  // create a nice string
  $title = implode(', ', $clean);
  // send it back
  return $title;
}

/**
 * Fix Gravity Form Tabindex Conflicts
 *
 * @see http://gravitywiz.com/fix-gravity-form-tabindex-conflicts/
 *
 * @param $tab_index
 * @param bool $form
 *
 * @return int
 */
function gform_tabindexer( $tab_index, $form = false ) {
  $starting_index = 1000; // if you need a higher tabindex, update this number
  if ( $form ) {
    add_filter( 'gform_tabindex_' . $form['id'], __NAMESPACE__ . '\\gform_tabindexer' );
  }
  return \GFCommon::$tab_index >= $starting_index ? \GFCommon::$tab_index : $starting_index;
}
add_filter( 'gform_tabindex', __NAMESPACE__ . '\\gform_tabindexer', 10, 2 );


/**
 * Return the name of the post type.
 *
 * This function exists solely because we are displaying the name "Insights" instead
 * of posts. It'll return the name of the current post type, unless it is a post, in
 * which case it will return "Insights"
 *
 * @return string
 */
function postTypeName() {
  $type = get_post_type();
  $type === 'post' ? $return = 'Insights' : $return = ucwords($type);
  return $return;
}


/**
 * Returns an array with meta data for a project
 * (ie. client name, client logo, etc.)
 *
 * @param null $id
 *
 * @return array
 */
function projectMetaDisplay($id = null) {
  // check if ID is set
  if (!$id) {
    global $post;
    $id = $post->ID;
  }
  // init an array
  $projectMeta = [
    'client_set' => false,
    'client_name' => false,
    'has_logo' => false,
    'client_logo' => false,
    'project_terms' => false
  ];
  // init some vars
  $clientSet = $clientName = $hasLogo = $clientLogo = false;
  // this returns the post ID of the *client* attached to a project
  $clientID  = get_field( 'project_select_client' );
  // store a boolean indicating if the clent has been set
  if($clientID) {
    $projectMeta['client_set'] = true;
  }
  // get the client name
  $projectMeta['client_name'] = get_the_title($clientID);
  // see if a logo has been set for the client
  if ( has_post_thumbnail( $clientID ) ) {
    $projectMeta['has_logo']    = true;
    // added in an option to upload a secondary logo to use on the hero images.
    // check and see if we are in a single project view and if an alternate logo has been uploaded
    if (is_singular('projects') && get_field( 'hero_image_logo', $clientID )) {
      // grab the secondary logo
      $imageArray = get_field( 'hero_image_logo', $clientID );
      $image = '<img src="' . $imageArray['url'] . '" alt="' . $imageArray['alt'] . '">';
      // save it to our array
      $projectMeta['client_logo'] = $image;
    } else {
      // grab the featured image
      $projectMeta['client_logo'] = get_the_post_thumbnail( $clientID );
    }
  }
  // get the tags associated with the project
  $projectMeta['project_terms'] = projectTags($id);
  // return the array
  return $projectMeta;
}

/**
 * Get the tags associated with a project
 * Returns the terms list as an unordered list, unstyled display inline
 * @param $id
 *
 * @return null|string
 */
function projectTags($id) {
  // project tags is a custom taxonomy. Get them.
  $terms = get_the_terms($id, 'project-tags');
  // set a null var because it just seems like a wise thing to do.
  $termList = null;
  // if there are terms and there's no weird issues
  if ( $terms && ! is_wp_error( $terms ) ) :
    // build a list of terms. Ideally I would have just returned an array
    // of strings but in this case it was easier to just pass in the desired
    // format and return the output to the template
    $termList = '<ul class="project-tags list-unstyled list-inline">';
    foreach ($terms as $term) {
      $termList .= '<li>' . $term->name . '</li>';
    }
    $termList .= '</ul>';

  endif;
  // return the formatted list lf terms
  return $termList;
}

/**
 * Get content from a page or post by ID
 *
 * @param $id
 *
 * @return mixed|void
 */
function get_content( $id ) {
  // get post content by ID, apply filters
  $content = apply_filters( 'the_content', get_post_field( 'post_content', $id ) );
  // return
  return $content;
}

/**
 * Custom [share] shortcode template
 * @return string
 */
function custom_roots_share_buttons_template() {
  return get_template_directory() . '/templates/shortcode-share.php';
}
add_action( 'roots/share_template', __NAMESPACE__ . '\\custom_roots_share_buttons_template' );
/**
 * Remove Roots Share Buttons assets
 */
function remove_roots_share_buttons_assets() {
  wp_dequeue_style( 'roots-share-buttons' );
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\remove_roots_share_buttons_assets' );

/**
 * Generate a post excerpt.
 *
 * When trying to get a post excerpt outside the loop,
 * wp_trim_excerpt filter will return the excerpt for the current post
 * instead of the targeted post, which is ridiculously annoying.
 *
 * Use this to grab an excerpt outside of the loop.
 *
 * @param null $postID
 * @param int $words
 *
 * @return mixed|void
 */
function customExcerpt($postID = null, $words = 25) {

  $text = get_post_field('post_content', $postID);
  $excerpt = wp_trim_words($text, $words);
  return apply_filters('get_the_excerpt', $excerpt, $postID);

}

/**
 * Auto-wrap video embeds added via the_content in a div.
 *
 * Videos added via the content editor break the layout unless
 * wrapped in a div with the class "video-wrap". This function
 * adds the wrapping div at the time the video is added so the
 * content creator does not have to fiddle with html.
 *
 * Note - this action will not affect existing videos as the filter
 * runs when the data is pulled from the oEmbed provider and the
 * return is cached as post meta.
 *
 * The codex / developer docs on this are mostly useless. See comments
 * on number 4, and answer on number 3.
 *
 * @see http://wordpress.stackexchange.com/a/195135/48634
 *
 * @param $html
 * @param $url
 * @param $attr
 * @param $post_id
 *
 * @return string
 */
function embedWrapper($html, $url, $attr, $post_id) {
  return '<div class="video-wrap">' . $html . '</div>';
}

add_filter('embed_oembed_html', __NAMESPACE__ . '\\embedWrapper', 10, 4);

/******* JetPack ******/

/*
 * Remove JetPack devicepx
 */
/**
 *
 */
function dequeue_devicepx() {
  if(class_exists('Jetpack')) {
    wp_dequeue_script( 'devicepx' );
  }
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\dequeue_devicepx', 20 );
/*
 * Remove JetPack Styles
 */
// First, make sure Jetpack doesn't concatenate all its CSS
/**
 *
 */
function jetpackImplode() {
  if(class_exists('Jetpack')) {
    add_filter( 'jetpack_implode_frontend_css', '__return_false' );
  }
}
add_action('wp', __NAMESPACE__ . '\\jetpackImplode');

// Then, remove each CSS file, one at a time
/**
 *
 */
function nuke_jetpack_css() {
  if(class_exists('Jetpack')) {
    wp_deregister_style( 'AtD_style' ); // After the Deadline
    wp_deregister_style( 'jetpack_likes' ); // Likes
    wp_deregister_style( 'jetpack_related-posts' ); //Related Posts
    wp_deregister_style( 'jetpack-carousel' ); // Carousel
    wp_deregister_style( 'grunion.css' ); // Grunion contact form
    wp_deregister_style( 'the-neverending-homepage' ); // Infinite Scroll
    wp_deregister_style( 'infinity-twentyten' ); // Infinite Scroll - Twentyten Theme
    wp_deregister_style( 'infinity-twentyeleven' ); // Infinite Scroll - Twentyeleven Theme
    wp_deregister_style( 'infinity-twentytwelve' ); // Infinite Scroll - Twentytwelve Theme
    wp_deregister_style( 'noticons' ); // Notes
    wp_deregister_style( 'post-by-email' ); // Post by Email
    wp_deregister_style( 'publicize' ); // Publicize
    wp_deregister_style( 'sharedaddy' ); // Sharedaddy
    wp_deregister_style( 'sharing' ); // Sharedaddy Sharing
    wp_deregister_style( 'stats_reports_css' ); // Stats
    wp_deregister_style( 'jetpack-widgets' ); // Widgets
    wp_deregister_style( 'jetpack-slideshow' ); // Slideshows
    wp_deregister_style( 'presentations' ); // Presentation shortcode
    wp_deregister_style( 'jetpack-subscriptions' ); // Subscriptions
    wp_deregister_style( 'tiled-gallery' ); // Tiled Galleries
    wp_deregister_style( 'widget-conditions' ); // Widget Visibility
    wp_deregister_style( 'jetpack_display_posts_widget' ); // Display Posts Widget
    wp_deregister_style( 'gravatar-profile-widget' ); // Gravatar Widget
    wp_deregister_style( 'widget-grid-and-list' ); // Top Posts widget
    wp_deregister_style( 'jetpack-widgets' ); // Widgets
  }
}
add_action( 'wp_print_styles', __NAMESPACE__ . '\\nuke_jetpack_css' );

/**
 * Responsive Image Helper Function
 *
 * @param string $image_id the id of the image (from ACF or similar)
 * @param string $image_size the size of the thumbnail image or custom image size
 * @param string $max_width the max width this image will be shown to build the sizes attribute
 * @return string
 *
 * Credit / adapted from -
 * @url http://aaronrutley.com/responsive-images-in-wordpress-with-acf/
 *
 */

function ar_responsive_image($image_id,$image_size,$max_width){

  // check the image ID is not blank
  if($image_id != '') {

    // set the default src image size
    $image_src = wp_get_attachment_image_url( $image_id, $image_size );

    // set the srcset with various image sizes
    $image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

    $image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true);

    // generate the markup for the responsive image
    return '<img src="' . $image_src . '" srcset="' . $image_srcset . '" sizes="(max-width: ' . $max_width . ') 100vw, ' . $max_width . '" alt="' . $image_alt . '">';

  }
}

// Remove SEO Framework credits from source.
add_filter( 'the_seo_framework_indicator', '__return_false' );
