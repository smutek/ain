<?php

use Roots\Sage\Assets;


/**
 * Add a body class for pages using load more
 *
 * @param $classes
 *
 * @return array
 */
function loadMoreClass( $classes ) {
  // If on the insights page or on the news archive
  if ( is_home() || is_post_type_archive( 'news' ) ) {
    // add the class "infinite-load" to the body
    $classes[] = 'infinite-load';
  }
  return $classes;
}
add_filter( 'body_class', 'loadMoreClass' );

/**
 * Localize scripts
 * Set up variables to pass on to our script
 *
 * Creates a javascript object variable called ajaxLoadMore,
 * any of the variables in the args array can be accessed from
 * the script by using ajaxLoadMore.var-name - this lets us pass
 * dynamic user generated variables from php to our script/
 */
function localizeJS() {
  // make sure this is the insights page or news page
  if ( is_home() || is_post_type_archive( 'news' ) ) {
    // set the post type to be passed for the query
    is_home() ? $postType = 'post' : $postType = 'news';
    // If on insights we need to add 1 to each query to account
    // for the additional post on initial load
    $postType === 'post' ? $offset = 1 : $offset = 0;
    // Get the posts per page set by the user on the backend
    $postsPerPage = get_option( 'posts_per_page' );
    // The number of posts present on initial page load.
    $startingNumber = $postsPerPage + $offset;
    // Get the total number of posts
    $countPosts = wp_count_posts( $postType );
    $totalPosts = $countPosts->publish;

    // Set up a default query
    $query = array(
      'post_type'      => $postType,
      'posts_per_page' => $postsPerPage,
    );

    // Store the stuff to be passed to the script
    $args = array(
      'url'   => admin_url( 'admin-ajax.php' ),
      'query' => $query,
      'postsPerPage' => $postsPerPage,
      'offsetBy' => $offset,
      'totalPosts' => $totalPosts,
      'startingNumber' => $startingNumber
    );
    // Sage JS needs to be registered in order to use localize script
    wp_register_script( 'sage/js', Assets\asset_path( 'scripts/main.js' ), [ 'jquery' ], null, true );
    wp_localize_script( 'sage/js', 'ajaxLoadMore', $args );
  }
}
add_action( 'wp_enqueue_scripts', 'localizeJS' );

/**
 * AJAX Load More
 *
 * This is the callback function. This is what gets  ran each time the load more button is clicked.
 *
 */
function ajaxLoadMore() {
  $args                = isset( $_POST['query'] ) ? array_map( 'esc_attr', $_POST['query'] ) : array();
  $args['post_type']   = isset( $args['post_type'] ) ? esc_attr( $args['post_type'] ) : 'post';
  $args['post_status'] = 'publish';
  $args['offset']      = esc_attr($_POST['offset']);
  $args['posts']       = esc_attr($_POST['posts']);

  ob_start();
  $loop = new WP_Query( $args );
  if ( $loop->have_posts() ):
    echo '<div class="new-posts animated fadeInUp">';
    while ( $loop->have_posts() ): $loop->the_post();
      get_template_part( 'templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format() );
    endwhile;
    echo '</div>';
  endif;
  wp_reset_postdata();
  $data = ob_get_clean();
  wp_send_json_success( $data );
  wp_die();
}
add_action( 'wp_ajax_ajaxLoadMore', 'ajaxLoadMore' );
add_action( 'wp_ajax_nopriv_ajaxLoadMore', 'ajaxLoadMore' );
