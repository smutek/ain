<?php
namespace Roots\Sage\Modal;

use Roots\Sage\Wrapper\SageWrapping;

/**
 * Class Modal
 * @package Roots\Sage\Modal
 *
 * Usage  - $formObject = new Modal('your_acf_field', 'form');
 *
 * This will instantiate a gravity form object and output a modal with the correct form
 * immediately after the footer.
 *
 * On other templates you can use the formID method to get your forms ID and build a link, like this -
 * $formID    = $formObject->formID();
 *
 * Generate a button like this -
 * $button    = $formObject->renderButton();
 */
class Modal {

  /**
   * // ACF Field value being passed to the class
   * @var
   */
  protected $field;

  /**
   * // Type of modal being called
   * @var
   */
  protected $type;

  /**
   * Types of modals available
   * @var
   */
  private $modalTypes;

  /**
   * Gravity forms form object
   * @var
   */
  protected $formObject;

  /**
   * ID of a gravity form
   * @var
   */
  public $formID;

  /**
   * Modal constructor.
   *
   * Both field and type are required.
   *
   * @param $field
   * @param $type
   */
  public function __construct( $field, $type ) {

    // Check to make sure ACF and Gravity Forms are both active.
    // bail if ACF not active.
    if ( ! class_exists( 'acf' ) ) {
      return;
    }
    // bail if we are calling a form and gforms is not active
    if ( $type === 'form' && ! class_exists( 'GFCommon' ) ) {
      return;
    }
    // bail if we are calling anything but a form, since I have not done video yet
    if ( ! $type === 'form' ) {
      return;
    }
    // store the ACF field as a property of the object instance
    $this->field = $field;
    // store the type string as a property of the object instance
    $this->type  = $type;

    // render the modal to the footer area. Uses the outputModal method.
    add_action( 'after_footer', [ $this, 'outputModal' ] );
  }
  // **** end __constructor

  /**
   * Array of modal types available
   *
   * Originally the idea was to use this class for both video modals
   * and form modals. As of now it has only been implemented on form
   * modals.
   *
   * @return array
   */
  private function defineTypes() {
    $types = [
      'video',
      'form'
    ];

    return $types;
  }

  /**
   * Builds the file path to the modal template
   *
   * @return string
   */
  public function filePath() {
    $type     = $this->type;
    $path     = 'templates/modules/modals/modal-' . $this->type . '.php';
    $template = new SageWrapping( $path );

    return $template;
  }

  /**
   * Returns a gravity forms form object
   *
   * @return mixed|null|void
   */
  public function formObject() {
    $field      = $this->field;
    $formObject = get_field( $field );
    return $formObject;
  }

  /**
   * Returns the ID of the form
   *
   * @return mixed
   */
  public function formID() {

    $formObject = $this->formObject();
    $formID     = $formObject['id'];

    return $formID;
  }

  /**
   * Returns the rendered modal template markup
   * @return string
   */
  public function renderModal() {
    ob_start();
    include $this->filePath();
    $output = ob_get_clean();

    return $output;
  }

  /**
   * Echos the rendered modal template markup
   *
   */
  public function outputModal() {
    $modal = $this->renderModal();
    echo $modal;
  }

  /*
   * Generate button and markup
   */
  public function renderButton() {
    // Get the form ID
    $formID = $this->formID();
    // Build the modal link
    $modalLink   = "#form-modal-" . $formID;
    // Build our HTML
    $button = '<div class="form-trigger">';
    $button .= '<a class="btn btn-primary" href="' .  $modalLink . '" data-toggle="modal" data-target="' .  $modalLink . '">Apply Now</a>';
    $button .= '</div>';
    // Return the button
    return $button;
  }

}

