<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;
use Roots\Sage\Extras;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  //add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage')
  ]);
  register_nav_menus([
    'footer_navigation' => __('Footer Navigation', 'sage')
  ]);
  register_nav_menus([
    'social_navigation' => __('Social Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // project thumbnails
  add_image_size( 'project_large', 950, 950, true );
  add_image_size( 'project_medium', 450, 450, true);
  add_image_size( 'project_240', 240, 240, true);
  add_image_size( 'project_256', 256, 256, true);
  add_image_size( 'project_350', 350, 350, true);
  add_image_size( 'project_550', 550, 550, true);
  add_image_size( 'avatar', 40, 40, true);

  // blog thumbnails
  add_image_size( 'blog_full', 1080, 727, true);
  add_image_size( 'blog_large', 725, 488, true);
  add_image_size( 'blog_half', 363, 244, true);
  add_image_size( 'blog_third', 241, 163, true);

  //  portfolio gallery
  //  Small Square - 460x460px
  //  Large Square - 946x946px
  //  Rectangle - 946x460px
  add_image_size( 'small_square', 460, 460, true);
  add_image_size( 'large_square', 946, 946, true);
  add_image_size( 'rectangle', 946, 460, true);

  // custom logo support
  // https://codex.wordpress.org/Theme_Logo
  add_theme_support( 'custom-logo' );

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Sidebar', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = in_array(true, [
    // The sidebar WILL be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    // Normal sidebar logic is reversed. If you need a sidebar
    // Add your conditional to the array.
    // For example:
    // is_404(),
    // is_front_page(),
    // is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  if(is_page_template('template-about.php')) {
    wp_enqueue_script('masonry', '', ['jquery']);
  }

  wp_enqueue_script( 'modernizr', Assets\asset_path( 'scripts/modernizr.js' ), [], null, false );
  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);

  if(Extras\hasVideoModal()) {
    wp_enqueue_script('wp-mediaelement');
    wp_enqueue_style( 'wp-mediaelement' );
  }
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

// remove  plugin assets
  function removeAssets() {
    // remove simple instagram plugin assets
    if(class_exists('simple_instagram')) {
      wp_dequeue_style('fontawesome');
      wp_dequeue_style('si-style');
    }
  }
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\removeAssets', 9999);

  function tagManager() {

    get_template_part('templates/modules/module', 'tag-manager-head');


  }
  add_action('wp_head', __NAMESPACE__ . '\\tagManager');





