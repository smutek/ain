<?php

namespace Roots\Sage\Titles;

/**
 * Page titles
 */
/**
 * @return string|void
 */
function title() {
  if ( is_home() ) {
    if ( get_option( 'page_for_posts', true ) ) {
      return get_the_title( get_option( 'page_for_posts', true ) );
    } else {
      return __( 'Latest Posts', 'sage' );
    }
  } elseif ( is_archive() ) {

    $title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );

    if ( is_post_type_archive( 'careers' ) ) {
      $title = 'Careers At Ainsley';
    }

    return $title;
  } elseif ( is_search() ) {
    return sprintf( __( 'Search Results for %s', 'sage' ), get_search_query() );
  } elseif ( is_404() ) {
    return __( 'Not Found', 'sage' );
  } else {
    return get_the_title();
  }
}



/**
 * Post Headings
 *
 * Returns the post title wrapped in an H2 or H3 depending on
 * which page it is called from. Pass classes in as a string,
 * or as an array.
 *
 * @param string $class
 *
 * @return string
 */
function postHeading( $class = '' ) {

  $title  = get_the_title();
  $id     = get_the_ID();
  $postID = 'post-' . $id;

  // see if classes have been added
  if ( $class ) {
    // if strings were passed, parse into an array
    if ( ! is_array( $class ) ) {
      $class = preg_split( '#\s+#', $class );
    }
    // escape the array
    $classArray = array_map( 'esc_attr', $class );
    // Separates classes with a single space
    $classes = 'class="' . join( ' ', $classArray ) . '"';
  } else {
    // Ensure that we always coerce class to being an array.
    $class   = array();
    // stomp the class array so it does not output
    $classes = null;
  }

  $h2 = "<h2 id=\"{$postID}\" {$classes}>" . $title . "</h2>";
  $h3 = "<h3 id=\"{$postID}\" {$classes}>" . $title . "</h3>";
  // if we are on the blog page return h2, otherwise return h3
  is_home() ? $return = $h2 : $return = $h3;
  // send back the results
  return $return;

}
