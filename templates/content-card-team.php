<?php
use Roots\Sage\Extras;
// repeater, pass field and subfield, returns string (see function comments in extras.php)
$title = Extras\teamTitles('add_titles', 'team_add_title');
?>

<article <?php post_class('col-sm-6 col-md-3'); ?>>

    <?php
    if(has_post_thumbnail()): ?>
    <div class="thumbnail-image">
      <a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail('full'); ?>
      </a>
    </div>
    <?php endif; ?>
    <header>
      <a class="entry-link" href="<?php the_permalink(); ?>">
        <h3 class="entry-title"><?php the_title(); ?><small><?= $title; ?></small></h3>
      </a>
    </header>
</article>
