<?php
$id = get_the_ID();
$postID = 'post-' . $id;
$linkID = 'link-' . $id;
// setup the title
$title = get_the_title();
$subhead = get_field( 'post_add_subhead' );

if(!empty($subhead)) {
  $title = get_the_title() . ' <small>' . $subhead . '</small>';
}
?>
<div class="col-sm-12 career-wrap">
  <article <?php post_class(); ?>>
    <header>
      <a href="<?php the_permalink(); ?>">
        <h2 class="entry-title"><?= $title; ?></h2>
      </a>
    </header>
    <div class="entry-summary">
      <?php the_excerpt(); ?>
    </div>
    <footer>
      <a id="<?= $linkID; ?>" href="<?= get_the_permalink(); ?>" aria-labelledby="<?= $linkID . ' ' . $postID; ?>">Learn More</a>
    </footer>
  </article>
</div>
