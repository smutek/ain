<?php

use Roots\Sage\Titles;
use Roots\Sage\Extras;
// returns title wrapped in heading tags.
// Pass in classes as single strings or as an array
$title = get_the_title();

$id = get_the_ID();
$postID = 'post-' . $id;
$linkID = 'link-' . $id;
?>

<article <?php post_class('card news-card'); ?>>
  <?php
  // todo-jimmy Fix excerpt output
  // Changed heading level to H3 to use on the home page.
  // Either need to throw some logic in this template to
  // swap out h2 and h3 as needed, or make another template
  // TBD at a later date/
  if(has_post_thumbnail()): ?>
    <div class="thumbnail-image featured-image">
      <a href="<?= get_the_permalink(); ?>">
        <?php the_post_thumbnail(); ?>
      </a>
    </div>
  <?php endif; ?>
  <div class="card-content">
    <header class="card-header entry-header">
      <a href="<?= get_the_permalink(); ?>">
        <h2><?= $title; ?></h2>
      </a>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-summary">
      <?php the_excerpt(); ?>
    </div>
    <footer class="read-more">
      <a id="<?= $linkID; ?>" href="<?= get_the_permalink(); ?>" aria-labelledby="<?= $linkID . ' ' . $postID; ?>">Full Post</a>
    </footer>
  </div>
</article>

