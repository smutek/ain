<div class="description">
  <?php the_content(); ?>
</div>
<?php
if ( have_rows( 'add_capabilities' ) ) :
  ?>
  <div class="capabilities">
    <ul class="list-unstyled">
      <?php while ( have_rows( 'add_capabilities' ) ) : the_row();

        $titleOnly = get_sub_field( 'capability_title_only' );
        // add a class to the item(s) with title only checked
        $titleOnly ? $class = 'message-block' : $class = 'capability';
        $title = get_sub_field( 'capability_title' );
        $icon  = get_sub_field( 'capability_icon' );
        $text  = get_sub_field( 'capability_text' );
        ?>
        <li class="<?= $class; ?>">
          <?php if ( ! $titleOnly ) : ?>
            <img src="<?= $icon['url']; ?>" alt="<?= $icon['alt']; ?>">
          <?php endif; ?>
          <h2><?= $title; ?></h2>
          <?php if ( ! $titleOnly ) : ?>
            <div class="copy">
              <?= $text; ?>
            </div>
          <?php endif; ?>
        </li>
      <?php endwhile; // end while have rows
      ?>
    </ul>
  </div>
<?php endif; // end if have rows

  get_template_part('templates/modules/module', 'photo-gallery');
