<?php

$address1 = get_theme_mod( 'address_line_1' );
$address2 = get_theme_mod( 'address_line_2' );
$phone = get_theme_mod( 'phone_setting' );

$form_object = get_field('select_contact_form');
gravity_form_enqueue_scripts($form_object['id'], true);
?>
<div class="row contact-wrap">
  <div class="col-md-6 content-wrap">
    <?php get_template_part('templates/page', 'header'); ?>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <div class="form">
      <?php gravity_form($form_object['id'], false, false, false, '', true, 1); ?>
    </div>
    <div class="contact-info">
      <p class="address"><?= $address1; ?><br><?= $address2; ?></p>
      <p><a href="tel:<?= $phone; ?>"><?= $phone; ?></a></p>
    </div>
  </div>
  <div class="col-md-6 map-wrap">
    <div class="map">
      <?php get_template_part('templates/modules/module', 'map-display'); ?>
    </div>
  </div>
</div>




