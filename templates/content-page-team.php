<?php
// radio button - string (title, date, or rand - defaults to rand)
// used for our team query
$orderby = get_field( 'our_team_display_order' );
// returns bool / true false
$showTestimonial = get_field( 'display_a_testimonial' );
?>

<h1 class="sr-only"><?php the_title(); ?></h1>

<?php get_template_part( 'templates/modules/module', 'video-hero' ); ?>

<div class="container">
  <?php if ( have_rows( 'select_leadership_team' ) ) : ?>
    <div class="team-listing leadership-listing">
      <h2>Leadership</h2>
      <?php while ( have_rows( 'select_leadership_team' ) ) : the_row();

        $post_object = get_sub_field( 'team_member' );

        if ( $post_object ):
          // override $post
          $post = $post_object;
          setup_postdata( $post );

          get_template_part( 'templates/content-card', 'team' );

          wp_reset_postdata(); // reset $post object
        endif; // end post object
      endwhile; // end while have rows (leadership repeater)
      ?>
    </div>
  <?php endif; // end if have rows (leadership repeater)

  // now set up the query for the rest of the team.
  // Orderby var is set via ACF and populated at the top of the page
  $args = [
    'post_type'      => 'team',
    'posts_per_page' => - 1,
    'no_found_rows'  => true,
    'orderby'        => $orderby,
    'tax_query'      => [
      [
        'taxonomy' => 'teams',
        'terms'    => 'leadership',
        'field'    => 'slug',
        'operator' => 'NOT IN'
      ]
    ]
  ];

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) : ?>
    <div class="team-listing">
      <h2>Our Team</h2>
      <?php
      while ( $query->have_posts() ): $query->the_post();

        get_template_part( 'templates/content-card', 'team' );

      endwhile;
      wp_reset_postdata();
      ?>
    </div>
  <?php endif; // end if have posts ?>
</div>
<?php if($showTestimonial) {
  get_template_part('templates/modules/module-testimonial', 'display');
}

// todo-jimmy wrap in conditional check after setup
get_template_part('templates/modules/module', 'banner');







