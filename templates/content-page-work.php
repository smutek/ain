<?php
use Roots\Sage\Extras;
use Roots\Sage\Titles;

$header = get_field( 'our_clients_heading' );

$content = get_the_content();
  ?>

<?php if($content !== "") : ?>
  <div class="container work-intro">
    <?= $content; ?>
  </div>
<?php endif; ?>
<?php get_template_part( 'templates/modules/module', 'home-projects' ) ?>

<?php if ( have_rows( 'add_clients' ) ) : ?>
  <section class="clients-list">
    <div class="container">
      <header>
        <h2><?= $header; ?></h2>
      </header>
      <div class="row">
        <?php
        while ( have_rows( 'add_clients' ) ) : the_row();
          $clientID = get_sub_field( 'add_client' );
          if ( has_post_thumbnail( $clientID ) ) :
            ?>
            <div class="col-xs-6 col-sm-4 client-logo">
              <div class="thumbnail-image">
                <?= get_the_post_thumbnail( $clientID ); ?>
              </div>
            </div>
          <?php endif; // end if has post thumbnail ?>
        <?php endwhile; // end while have rows ?>
      </div>
    </div>
  </section>
<?php endif; // end if have rows
