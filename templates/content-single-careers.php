<?php
use Roots\Sage\Modal\Modal;

while ( have_posts() ) : the_post();

  // setup the title
  $title   = get_the_title();
  $subhead = get_field( 'post_add_subhead' );
  $eoe     = get_theme_mod( 'eoe_statement' );

  if ( ! empty( $subhead ) ) {
    $title = get_the_title() . ' <small>' . $subhead . '</small>';
  }

  // sets up the back to all careers link at the bottom
  $type   = get_post_type();
  $backTo = get_post_type_archive_link( $type );
  // This is used to display associated team members
  $teamID = get_field( 'career_choose_team' );
  // Output the contact form. Calling this class on single careers
  // will output the trigger button below the content and the
  // modal and form below the <footer>
  $modalObject = new Modal( 'career_choose_form', 'form' );
  $formButton  = $modalObject->renderButton();

  $responsibilities = get_field( 'career_responsibilities' );
  $qualifications   = get_field( 'career_qualifications' );
  ?>
  <article <?php post_class(); ?>>

    <header>
      <h1 class="entry-title"><?= $title; ?></h1>
    </header>

    <div class="entry-content">
      <?php the_content(); ?>

      <?php if ( have_rows( 'career_descriptions' ) ) :
        while ( have_rows( 'career_descriptions' ) ) : the_row();
          $heading = get_sub_field( 'career_description_heading' );
          $copy    = get_sub_field( 'career_decsription_copy' );
          ?>
          <?php if ( $heading ) : ?>
            <h2><?= $heading; ?></h2>
          <?php endif; ?>

          <?php if ( $copy !== "" ) : ?>
            <div class="responsibilities">
              <?= $copy; ?>
            </div>
          <?php endif; ?>

        <?php endwhile; // end while descriptions
      endif; // end if descriptions ?>

      <?php if ( $eoe ) : ?>
        <div class="eoe" style="margin-bottom: 40px;">
          <small><?= $eoe; ?></small>
        </div>
      <?php endif; ?>

      <?= $formButton; ?>
    </div>

    <div class="entry-footer">
      <?php /*
	   * Associated team members. Just a simple loop, grab all team members
	   * based on the selected taxonomy term.
	   */
      if ( $teamID ) :
        // Get the nice name for the term
        $teamName = get_term( $teamID )->name;
        // Build a simple string: $Some Team
        $teamTitle = 'Our ' . $teamName . ' Team';
        // Query Args
        $args = [
          'post_type'      => 'team',
          'posts_per_page' => - 1,
          'no_found_rows'  => true,
          'tax_query'      => [
            [
              'taxonomy' => 'teams',
              'field'    => 'id',
              'terms'    => $teamID,
              'operator' => 'IN'
            ]
          ]
        ];
        // Query
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) :
          // output
          ?>
          <h2><?= $teamTitle; ?></h2>
          <ul class="list-unstyled team-members">
            <?php while ( $query->have_posts() ) : $query->the_post();
              // check for a thumbnail, because why bother otherwise
              if ( has_post_thumbnail() ) :
                ?>
                <li>
                  <a href="<?php the_permalink(); ?>" data-toggle="tooltip" data-placement="bottom auto"
                     title="<?php the_title(); ?>">
                    <?php the_post_thumbnail( 'thumbnail', [ 'class' => 'img-circle' ] ); ?>
                  </a>
                </li>
                <?php
              endif; // end thumbnail check
            endwhile; // end while have posts
            wp_reset_postdata(); // reset
            ?>
          </ul>
          <?php
        endif; // end if have posts
      endif; // end if team ID
      ?>
      <a class="back" href="<?= $backTo; ?>"><i class="fa fa-2x fa-angle-left" aria-hidden="true"></i>View all openings</a>
      <div class="share">
        <?= do_shortcode( '[share]' ); ?>
      </div>
    </div>
  </article>
<?php endwhile;
