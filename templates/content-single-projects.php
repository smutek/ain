<?php
while ( have_posts() ) : the_post();
// Using ACF flexible content here.
// It works similar to a repeater. In this case we are checking if we lave
// rows, then if so we check to see which layout is being called and load the
// appropriate template. All templates are in the templates/portfolio folder
// with the exception of the testimonial, which (as of today 12/3/16) uses
// the existing testimonial display template
  ?>
  <article <?php post_class(); ?>>
    <header class="entry-header">
      <h1 class="entry-title sr-only"><?php the_title(); ?></h1>
    </header>

    <?php get_template_part( 'templates/modules/module', 'video-hero' ); ?>

    <div class="entry-content">
      <?php
      if ( have_rows( 'portfolio_content_blocks' ) ) :
        while ( have_rows( 'portfolio_content_blocks' ) ) : the_row();

          if ( get_row_layout() === 'image_and_text_block' ) :
            get_template_part( 'templates/portfolio/image-and-text' );

          elseif ( get_row_layout() === 'text_only_block' ) :
            get_template_part( 'templates/portfolio/text' );

          elseif ( get_row_layout() === 'video_block' ) :
            get_template_part( 'templates/portfolio/video' );

          elseif ( get_row_layout() === 'banner_block' ) :
            get_template_part( 'templates/portfolio/banner' );

          elseif ( get_row_layout() === 'testimonial_block' ) :
            get_template_part( 'templates/modules/module', 'testimonial-display' );
          endif;

        endwhile;
      endif;
      ?>
    </div>
    <footer class="prev-next-posts container-fluid">
      <div class="row">
        <?php get_template_part('templates/modules/module', 'prev-next-project'); ?>
      </div>
    </footer>
  </article>
<?php endwhile; ?>
