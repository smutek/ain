<?php
use Roots\Sage\Extras;

while ( have_posts() ) : the_post();
  // repeater, pass field and subfield, returns string (see function comments in extras.php)
  $title = Extras\teamTitles('add_titles', 'team_add_title');
  // url field, returns string
  $linkedIn = get_field( 'linkedin_url' );
  //image field, returns image array
  $profilePhoto = get_field( 'profile_photo' );
  // dropdown select, returns string (label)
  $faveHeader = get_field( 'favorite_things' );
  ?>
  <article <?php post_class(); ?>>
    <header class="page-header">
      <div class="container">
        <div class="team-info">
          <h1 class="entry-title">
            <?php the_title(); ?>
            <small>
              <?= $title; ?>
            </small>
          </h1>
          <a class="social-icon linkedin-icon" href="<?= $linkedIn; ?>" target="_blank"><i class="fa fa-linkedin"
                                                                                           aria-hidden="true"></i>
            <span class="sr-only">Connect with <?= get_the_title(); ?> on LinkedIn</span></a>
        </div>
        <div class="thumbnail-image">
          <img src="<?= $profilePhoto['url']; ?>" alt="<?= $profilePhoto['alt']; ?>">
        </div>
      </div>
    </header>
    <div class="entry-content container">
      <div class="row">
        <div class="team-bio col-xs-12 col-sm-9 col-md-9">
          <?php the_content(); ?>
        </div>
        <?php if ( have_rows( 'add_favorite_things' ) ) : ?>
          <aside class="team-faves col-xs-12 col-sm-3 col-md-3">
            <h2 class="favorite-things"><?= $faveHeader; ?></h2>
            <ul class="list-unstyled favorite-list">
              <?php while ( have_rows( 'add_favorite_things' ) ): the_row(); ?>
                <li><?= get_sub_field( 'a_favorite_thing' ); ?></li>
              <?php endwhile; ?>
            </ul>
          </aside>
        <?php endif; // end favorite things ?>
      </div>
    </div>
    <footer class="team-nav">
      <div class="container">
        <div class="row">
          <div class="col-sm-9">
            <div class="row">
              <div class="col-xs-6 previous">
                <?php previous_post_link('%link', '%title'); ?>
              </div>
              <div class="col-xs-6 next">
                <?php next_post_link('%link', '%title'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </article>
<?php endwhile; ?>
