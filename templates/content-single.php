<?php
use Roots\Sage\Extras;

while (have_posts()) : the_post();
$moreLink = Extras\postTypeName();
?>
  <article <?php post_class(); ?>>
    <header class="entry-header">
      <?php if(has_post_thumbnail()) : ?>
        <?php the_post_thumbnail(); ?>
      <?php endif; ?>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer class="prev-next-posts">
      <h2>More <?= $moreLink; ?></h2>
      <?php get_template_part('templates/modules/module', 'prev-next'); ?>
    </footer>
  </article>
<?php endwhile; ?>
