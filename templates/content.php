<?php

use Roots\Sage\Titles;
use Roots\Sage\Extras;
// returns title wrapped in heading tags.
// Pass in classes as single strings or as an array
$title = Titles\postHeading('entry-title');

$id = get_the_ID();
$postID = 'post-' . $id;
$linkID = 'link-' . $id;
$firstPost = Extras\isFirstPost();

$outerClasses = [
  'card-outer'
];

$firstPost ? $outerClasses = "col-sm-12 first card-outer" : $outerClasses = "col-sm-6 col-md-4 equal-height card-outer post-loading";

if(is_front_page()) {
  $outerClasses = "col-sm-4 equal-height card-outer";
}

$classes = [
  'card',
  'insights-card'
];

?>
<div class="<?= $outerClasses; ?>">
  <article <?php post_class($classes); ?>>
    <?php
    // todo-jimmy Fix excerpt output
    // Changed heading level to H3 to use on the home page.
    // Either need to throw some logic in this template to
    // swap out h2 and h3 as needed, or make another template
    // TBD at a later date/
    if(has_post_thumbnail()): ?>
      <div class="thumbnail-image featured-image">
        <a href="<?= get_the_permalink(); ?>">
          <?php the_post_thumbnail('blog_large'); ?>
        </a>
      </div>
    <?php endif; ?>
    <div class="card-content">
      <header class="card-header entry-header">
        <a class="title-link" href="<?= get_the_permalink(); ?>">
          <?= $title; ?>
        </a>
        <?php get_template_part('templates/entry-meta'); ?>
      </header>
      <div class="entry-summary">
        <?php the_excerpt(); ?>
      </div>
      <footer class="read-more">
        <a id="<?= $linkID; ?>" href="<?= get_the_permalink(); ?>" aria-labelledby="<?= $linkID . ' ' . $postID; ?>">Read More</a>
      </footer>
    </div>
  </article>
</div>
