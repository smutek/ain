<?php
/*
 * Note ******
 * $postsNav is a bool and is being set in the previous next module.
 * In the case where this template is being loaded by the prev next
 * module the $authorID will have already been set.
 *
 * If it's not being loaded by the previous next module then the
 * $postsNav check will return false and we will get the author ID
 * by usual methods.
 */
if ( ! $postsNav ) {
  $authorID = get_the_author_meta( 'ID' );
  $postDate = get_the_date();
}
$postsNav = null;
$date = $postDate;
$postDate = null;
// get the ID of the team post associated with this user
// If this isn't set then skip outputting the image and author name
// until a suitable fallback is in place
$teamID     = get_user_meta( $authorID, 'team_id', true );
$authorName = get_the_title( $teamID );
$avatarURL  = get_the_post_thumbnail_url( $teamID, 'avatar' );
// Build the title line
// Some employees have multiple titles. Some pages separate the titles
// with commas, some with pipes |  - so I used a repeater to add titles.
// Here in the meta I just loop through the repeater and get the titles,
// then output with implode.
//
// get the subfield
$titles = get_field( 'add_titles', $teamID );
// init an empty array
$titleArray = [];
// if field values
if ( $titles ) {
  // loop through and add each value to the array
  foreach ( $titles as $title ) {
    $titleArray[] = $title['team_add_title'];
  }
}
// use implode to construct the output string
// this feels kludgy but it's working
$titleLine = implode( ', ', $titleArray );

// Whether or not to show the author info.
// We are not going to output blank markup if author info isn't available,
// or on on news posts. So lets evaluate both conditions in one spot.

// if no Team ID or is a news post, display equals false, else display = true
! $teamID || get_post_type() === 'news' ? $display = false : $display = true;
?>
<div class="entry-meta">
  <?php if ( $display ) : ?>
    <div class="thumbnail-image profile-image">
      <img class="img-circle media-object" src="<?= $avatarURL; ?>"
           alt="<?= "Avatar image for post author, " . $authorName; ?>">
    </div>
  <?php endif; ?>
  <div class="meta-body">
    <?php if ( $display ) : ?>
      <p class="byline author vcard"><?= __( 'Posted By ', 'sage' ) . $authorName;
        if ( $titles ) {
          echo ', ' . $titleLine;
        } ?></p>
    <?php endif; ?>
    <time class="updated" datetime="<?= get_post_time( 'c', true ); ?>"><?= $date; ?></time>
  </div>
</div>
