<?php
$logo     = get_theme_mod( 'secondary_logo' );
$address1 = get_theme_mod( 'address_line_1' );
$address2 = get_theme_mod( 'address_line_2' );
$phone    = get_theme_mod( 'phone_setting' );
$email    = get_theme_mod( 'email_setting' );

if ( get_theme_mod( 'business_name' ) ) {
  $businessName = get_theme_mod( 'business_name' );
} else {
  $businessName = get_bloginfo( 'name' );
}
?>
<footer class="content-info">
  <div class="container">
    <div class="footer-content">
      <div class="row">
        <div class="col-sm-12 col-md-2 col-lg-2 footer-sections footer-logo-section">
          <?php if ( $logo ) : ?>
            <p class="sr-only"><?php bloginfo( 'name' ); ?></p>
            <img src="<?= $logo; ?>" alt="Ainsley Secondary Logo">
          <?php else: ?>
            <p><?php bloginfo( 'name' ); ?></p>
          <?php endif; ?>
        </div>
        <div class="col-sm-12 col-md-2 col-lg-2 footer-sections footer-menu-section">
          <nav class="footer-nav" role="navigation">
            <?php
            if ( has_nav_menu( 'footer_navigation' ) ) :
              wp_nav_menu( [ 'theme_location' => 'footer_navigation', 'menu_class' => 'nav' ] );
            endif;
            ?>
          </nav>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 footer-sections footer-info-section">
          <div class="footer-social">
            <h3>Follow Us</h3>
            <nav class="social-nav" role="navigation">
              <?php
              if ( has_nav_menu( 'social_navigation' ) ) :
                wp_nav_menu( [
                  'theme_location' => 'social_navigation',
                  'menu_class'     => 'nav nav-social',
                  'link_before'    => '<span class="sr-only">',
                  'link_after'     => '</span>'
                ] );
              endif;
              ?>
            </nav>
          </div>
          <div class="contact footer-contact">
            <h3>Get In Touch</h3>
            <ul class="list-unstyled">
              <?php if ( $address1 ) : ?>
                <li class="addess"><?= $address1 . '<br>' . $address2; ?></li>
              <?php endif;
              if ( $phone ) : ?>
                <li class="phone"><a href="tel:<?= $phone; ?>"><?= $phone; ?></a></li>
              <?php endif;
              if ( $email ) : ?>
                <li class="email"><a href="mailto:<?= $email; ?>"><?= $email; ?></a></li>
              <?php endif; ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 footer-sections footer-connect-section">
          <?php dynamic_sidebar( 'sidebar-footer' ); ?>
          <div class="copyright">
            <p>
              <small>&copy; <?= date( 'Y' ) . ' ' . $businessName; ?> All Rights Reserved</small>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
