<?php
use Roots\Sage\Extras;
?>
<header class="banner navbar navbar-inverse navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="lines"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url( home_url( '/' ) ); ?>">
        <?= Extras\site_brand(); ?>
      </a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right']);
      endif;
      ?>
    </nav>
  </div>
</header>

