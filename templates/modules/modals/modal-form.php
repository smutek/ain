<?php
use Roots\Sage\Extras;

/* ** Note **
 * For the home page, the modal markup is loaded in via a conditional call in base-front page (same for the video modal!)
 * This allows us to keep the modal markup at the bottom of the page.
 *
 * Also note - the first conditional is an attempt to keep all contact related form
 * modals in one page template. Woot!
 *
 * need to check if there is a modal selected from the target module
 * function call
 * get the field name
 * pass it to the modal template
 * generate a modal
 * return the modal to the target module base template
 *
 * get the field name that was passed to it
 * pass that in the the modal template
 *
 *
 */

// fieldname should be populated by the function

//is_singular('careers') ? $fieldName = 'career_choose_form' : $fieldName = 'contact_cta_select_form';

// vars
//$formObject = get_field($fieldName );

$formObject = $this->formObject();

// set the modal ID to be specific to the form, rather than the page.
$formID = $formObject['id'];
$modalID = "form-modal-" . $formID;
?>

<!-- Modal -->
<div class="modal form-modal fade" id="<?= $modalID; ?>" tabindex="-1" role="dialog" aria-labelledby="<?=$modalID; ?>Label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <?php
        gravity_form_enqueue_scripts($formID, true);
        gravity_form($formID, true, true, false, '', true, 1);
        ?>
      </div>
    </div>
  </div>
</div>

