<?php
use Roots\Sage\Extras;

/* ** Note **
 * For the home page, the modal markup is loaded in via a conditional call in base-front page
 * This allows us to keep the modal markup at the bottom of the page.
 */

// vars
$id = get_the_ID();
$modalID = "video-modal-" . $id;
//$video = Extras\videoLink('pop_up_video_url');
$video = get_field('popup_video_url');
$title = get_field('pop_up_video_title');
$titleClass = 'modal-title';
if (!$title) {
  // If title is left blank in back end generate a screen reader friendly title
  $title = 'Video for ' . get_the_title();
  $titleClass .= ' sr-only';
}
?>

<!-- Modal -->
<div class="modal video-modal fade" id="<?= $modalID; ?>" tabindex="-1" role="dialog" aria-labelledby="<?=$modalID; ?>Label">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="<?= $titleClass; ?>" id="<?= $modalID; ?>Label">Video title?</h4>
      </div>
      <div class="modal-body modal-video-wrap">
        <video class="modal-video" controls>
          <source src="<?= $video; ?>" type="video/mp4">
        </video>
      </div>
    </div>
  </div>
</div>
