<?php
use Roots\Sage\Modal\Modal;
// text field
$showBanner = get_field( 'show_a_banner' );

if($showBanner) :

$image = get_field( 'banner_image' );
$background = $image['url'];
$showHeading = get_field('banner_show_title');
$heading = get_field( 'banner_heading' );
// basic WYSIWYG
$showCopy = get_field( 'banner_show_copy' );
$copy = get_field( 'banner_copy' );
// checkbox bool: true / false
$isForm = false;
$showButton = get_field( 'banner_show_button' );
// if showing a button, see which type and get the other details
if($showButton) {
  $buttonText = get_field( 'banner_button_text' );
  // radio button value - string: internal, form or external
  $linkType = get_field( 'banner_button_link_type' );
  // pull appropriate field based on button selection

  switch ($linkType) {
    case 'internal':
      $link = get_field( 'banner_button_select_form' );
      break;
    case 'external':
      $link = get_field( 'banner_button_link_external' );
      break;
    case 'form':
      $isForm = true;
        $modalObject = new Modal('banner_button_select_form', 'form');
        $formID = $modalObject->formID();
        $modalID = "form-modal-" . $formID;
        $modalLink = "#" . $modalID;
        $link = $modalLink;
        //$modal = $modalObject->renderModal();
      break;
    default:
      $link = null;
  }
}
?>
<div class="module module-banner" style="background: url(<?= $background; ?>) center no-repeat">
  <div class="container">
    <?php if($showHeading) : ?>
      <header><h2><?= $heading; ?></h2></header>
    <?php endif; ?>
    <?php if($showCopy) : ?>
      <div class="banner-copy">
        <?= $copy; ?>
      </div>
    <?php endif; ?>
    <?php if($showButton) : ?>
      <footer>
        <?php if(!$isForm) : ?>
        <a class="btn btn-primary" href="<?= $link; ?>"><?= $buttonText; ?></a>
        <?php endif; ?>
        <?php if($isForm) : ?>
          <a class="btn btn-primary" href="<?= $modalLink; ?>" data-toggle="modal" data-target="<?= $modalLink; ?>"><?= $buttonText; ?></a>
        <?php endif; ?>
      </footer>
    <?php endif; ?>
  </div>
</div>

<?php endif; // end if show banner


