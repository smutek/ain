<?php
use Roots\Sage\Modal\Modal;
$title = get_field( 'contact_cta_heading' );
$buttonText = get_field( 'contact_cta_button_text' );
//$formObject = get_field( 'contact_cta_select_form' );
$modalObject = new Modal('contact_cta_select_form', 'form');
// set the modal ID to be specific to the form, rather than the page.
$formID = $modalObject->formID();
$modalID = "form-modal-" . $formID;
$modalLink = "#" . $modalID;
?>
<div class="module module-home-contact">
  <div class="container">
    <header>
      <h2><?= $title; ?></h2>
    </header>
    <footer>
      <a class="btn btn-primary" href="<?= $modalLink; ?>" data-toggle="modal" data-target="<?= $modalLink; ?>"><?= $buttonText; ?></a>
    </footer>
  </div>
</div>
