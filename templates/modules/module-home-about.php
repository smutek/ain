<?php
// text field
$heading = get_field( 'about_heading' );
// basic WYSIWYG
$copyPrimary   = get_field( 'about_copy_primary' );
$copySecondary = get_field( 'about_copy_secondary' );
$showSecondary = get_field( 'about_show_secondary_content' );
// if showing secondary  do 2 col, else do 1 col
$showSecondary ? $rows = "6" : $rows = "12";

// checkbox bool: true / false
$showButton = get_field( 'about_show_button' );
// if showing a button, see which type and get the other details
if ( $showButton ) {
  $buttonText = get_field( 'about_button_text' );
  // radio button value - string: internal or external
  $linkType = get_field( 'about_button_link_type' );
  // if link type = internal then link = page link field, else link = external link field
  $linkType = "internal" ? $link = get_field( 'about_button_link_page' ) : $link = get_field( 'about_button_link_external' );
}

?>
<div class="module module-home-about">
  <div class="container">
    <header><h2><?= $heading; ?></h2></header>
    <div class="module-copy">
      <div class="row">
        <div class="copy-one col-sm-12 col-md-<?= $rows; ?>">
          <?= $copyPrimary; ?>
        </div>
        <?php if ( $showSecondary ) : ?>
          <div class="copy-two col-sm-12 col-md-6">
            <?= $copySecondary; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <?php if ( $showButton ) : ?>
      <footer>
        <a class="btn btn-primary" href="<?= $link; ?>"><?= $buttonText; ?></a>
      </footer>
    <?php endif; ?>
  </div>
</div>
