<?php
/*
 * Standard stuff here with the exception of the instagram feed.
 * Regarding the Instagram feed - need to decide whether to use
 * a plugin or roll our own. Saving till last. Plugin is the safe
 * bet but also leaves us at the mercy of the developer and gives
 * less control over assets that are loaded, etc. etc.
 *
 * Going to try to roll my own if time permits.
 *
 * Regarding storing the URL. Easy out is to just store it here, but
 * it's really more of a global configuration option.
 * todo-jimmy Figure out how to show the instagram photos, whether plugin or otherwise
 * todo-jimmy Decide where / how to store the instgram URL.
 * todo-jimmy Remove comments as appropriate
 */

// text field
$heading = get_field( 'culture_heading' );
// basic WYSIWYG
$copyPrimary = get_field( 'culture_copy' );

$showInstagram = get_field( 'culture_show_instagram' );
// if showing secondary  do 2 col, else do 1 col
$showInstagram ? $rows = "6" : $rows = "12";
// how many instagram photos to show
$numInstagram  = get_field( 'num_instagram' );
$InstagramURL = get_field( 'instagram_follow_url' );

// checkbox bool: true / false
$showButton = get_field( 'culture_show_button' );
// if showing a button, see which type and get the other details
if ( $showButton ) {
  $buttonText = get_field( 'culture_button_text' );
  // radio button value - string: internal or external
  $linkType = get_field( 'culture_button_link_type' );
  // if link type = internal then link = page link field, else link = external link field
  $linkType = "internal" ? $link = get_field( 'culture_button_link_page' ) : $link = get_field( 'culture_button_link_external' );
}

?>
<div class="module module-home-culture">
  <div class="container">
    <div class="row">
      <div class="module-content col-sm-12 col-md-<?= $rows; ?>">
        <header><h2><?= $heading; ?></h2></header>
        <div class="module-copy">
          <?= $copyPrimary; ?>
        </div>
        <?php if ( $showButton || $showInstagram ) : ?>
          <footer>
            <?php if ( $showButton ) : ?>
              <a class="btn btn-primary btn-dark" href="<?= $link; ?>"><?= $buttonText; ?></a>
            <?php endif; // end if button ?>
            <?php if ( $showInstagram !== "" && $InstagramURL !== "" ) : ?>
              <a class="social" href="<?= $InstagramURL; ?>" target="_blank"><i class="fa fa-2x fa-instagram" aria-hidden="true"></i>
                Follow Us</a>
            <?php endif; // end if instagram ?>
          </footer>
        <?php endif; // end footer (instagram or button) check ?>
      </div>
      <?php if ( $showInstagram ) : ?>
        <div class="instagram col-sm-12 col-md-6">
          <?php
          if(class_exists('simple_instagram')) {
            simple_instagram($numInstagram);
          }
          ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>


