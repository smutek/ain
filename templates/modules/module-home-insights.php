<?php
// text field, returns string
$heading = get_field( 'home_posts_heading' );
// number field returns integer
$numPosts = get_field( 'home_number_of_posts' );
// if num posts is null default to 3
get_field( 'home_number_of_posts' ) === null ? $numPosts = 3 : $numPosts = get_field( 'home_number_of_posts' );
// returns category ID
$catID = get_field( 'home_post_category' );
// txt field, returns string
$buttonText = get_field( 'home_posts_button_text' );
// link to the appropriate category archive
// todo-jimmy This should not be hard coded. Come back and take care of this once you've sorted out how the archives are going to work.
$buttonLink = "/insights";
// query args
$args = [
  'post_type'      => 'post',
  'posts_per_page' => $numPosts,
  'no_found_rows'  => true
];

$query = new WP_Query( $args );
if ( $query->have_posts() ) :
  ?>

  <div class="module module-home-insights">
    <div class="container">
      <header>
        <h2><?= $heading; ?></h2>
      </header>
      <div class="module-content insights row">
        <?php while ( $query->have_posts() ) : $query->the_post();
          get_template_part( 'templates/content');
        endwhile;
        wp_reset_postdata(); ?>
      </div>
      <footer>
        <a class="btn btn-primary" href="<?= $buttonLink; ?>"><?= $buttonText; ?></a>
      </footer>
    </div>
  </div>
<?php endif; // end if have posts
