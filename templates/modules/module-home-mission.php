<?php
use Roots\Sage\Modal\Modal;
// text field
$heading = get_field( 'mission_heading' );
// basic WYSIWYG
$copy = get_field( 'mission_copy' );
// checkbox bool: true / false
$isForm = false;
$showButton = get_field( 'mission_show_button' );
// if showing a button, see which type and get the other details
if($showButton) {
  $buttonText = get_field( 'mission_button_text' );
  // radio button value - string: internal, form or external
  $linkType = get_field( 'mission_button_link_type' );
  // pull appropriate field based on button selection

  switch ($linkType) {
    case 'internal':
      $link = get_field( 'mission_button_select_form' );
      break;
    case 'external':
      $link = get_field( 'mission_button_link_external' );
      break;
    case 'form':
      $isForm = true;
        $modalObject = new Modal('mission_button_select_form', 'form');
        $formID = $modalObject->formID();
        $modalID = "form-modal-" . $formID;
        $modalLink = "#" . $modalID;
        $link = $modalLink;
        //$modal = $modalObject->renderModal();
      break;
    default:
      $link = null;
  }
}
?>
<div class="module module-home-mission">
  <div class="container">
    <header><h2><?= $heading; ?></h2></header>
    <div class="module-copy">
      <?= $copy; ?>
    </div>
    <?php if($showButton) : ?>
      <footer>
        <?php if(!$isForm) : ?>
        <a class="btn btn-primary" href="<?= $link; ?>"><?= $buttonText; ?></a>
        <?php endif; ?>
        <?php if($isForm) : ?>
          <a class="btn btn-primary" href="<?= $modalLink; ?>" data-toggle="modal" data-target="<?= $modalLink; ?>"><?= $buttonText; ?></a>
        <?php endif; ?>
      </footer>
    <?php endif; ?>
  </div>
</div>




