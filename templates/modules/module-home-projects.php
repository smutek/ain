<?php
use Roots\Sage\Extras;
/*
 * The projects section consists of one repeater field with post objects inside.
 * The entire div is wrapped in an if have rows check because why output the div if
 * there's no data, am I right?
 *
 * After the initial check for the post object there's data setup, but in essence we're
 * just grabbing a bunch of custom fields. Dig?
 *
 * An H2 is output because semantics and structure matter. Wrapped in an sr-only for
 * screen reader friendly hiding.
 */

if ( have_rows( 'select_projects' ) ) : ?>

  <div class="module module-home-projects">
    <div class="project-container">
      <header class="sr-only"><h2>Featured Projects</h2></header>
        <div class="project-row">
      <?php while ( have_rows( 'select_projects' ) ) : the_row();
        // while the repeater has post objects
        $post_object = get_sub_field( 'selected_project' );
        if ( $post_object ):
          // override $post
          $post = $post_object;
          setup_postdata( $post );

          // Set up display variables. Grabbing data from the post object.

          // Title - check for a custom field, fall back to the regular title if it's blank.
          // Why the extra fall back? Because if it's not here someone will ask for it, you bet.
          $title = get_field( 'project_title_override' );
          // fall back to post title if field is blank
          if ( $title === "" ) {
            $title = get_the_title();
          }
          // Project thumbnail. Returns an array. We can do neat stuff with this array. Like determine
          // which image size is returned. If you are wondering why I didn't use it, and I am displaying
          // all images at 940 x 940, it's because I forgot. Please, help a brother out.
          // If I didn't forget update this comment accordingly. Thanks!
          // todo-jimmy When styling be sure to output appropriate image sizes
          $imageField = get_field( 'project_cover_thumbnail' );
          $projectThumb = Extras\ar_responsive_image($imageField['ID'], 'project_medium', '450px');
          $link         = get_the_permalink();
          // client info.
          $clientID  = get_field( 'project_select_client' );
          $projectMeta = null;
          $clientSet = false;
          if ( $clientID ) {
            $clientSet = true;
            $projectMeta = Extras\projectMetaDisplay();
          }
          ?>
          <div class="project-cover col-xs-6 col-sm-4 col-md-4 col-lg-4">
            <div class="project-inner">
              <a class="project-reveal" href="<?= $link; ?>">
                <?php if ( $clientSet ) : ?>
                  <div class="client-info">
                    <p class="sr-only">Client: <?= $projectMeta['client_name']; ?></p>
                    <?php if ( $projectMeta['has_logo']) : ?>
                      <?= $projectMeta['client_logo']; ?>
                    <?php endif; // end if has logo ?>
                  </div>
                <?php endif; // endif client set
                ?>
                <div class="project-info">
                  <h3><?= $title; ?></h3>
                  <?= $projectMeta['project_terms']; ?>
                </div>
              </a>
            </div>
            <div class="project-thumb">
              <?= $projectThumb; ?>
            </div>
          </div>
          <?php
          wp_reset_postdata(); // reset $post object
        endif; // end post object
      endwhile; // end while have rows (projects) ?>
        </div>
    </div>
  </div>
<?php endif; // end if have rows (projects)
