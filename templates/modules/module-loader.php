<?php
use Roots\Sage\Assets;
$svg = Assets\asset_path('images/ainsley-logo-alt.svg');
?>
<div class="loading">
  <div class="loader">
    <?php echo file_get_contents($svg); ?>
  </div>
  <span class="sr-only">Loading...</span>
</div>
