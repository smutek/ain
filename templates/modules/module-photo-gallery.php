<?php
$showGallery = get_field( 'show_photo_gallery' );
$images = get_field( 'gallery' );
$description = null;
$title = null;
// make sure show gallery is checked and there are images
if($showGallery && $images) :
  $title = get_field( 'gallery_header' );
  $class = "gallery-title";
  if ($title === "") {
    $title = get_the_title() . ': Photo Gallery';
    $class .= ' .sr-only';
  }

  $description = get_field( 'gallery_copy' );

?>
<section class="photo-gallery">

  <?php if($title) : ?>
    <h2 class="<?= $class; ?>"><?= $title; ?></h2>
  <?php endif; ?>
  <?php if($description) : ?>
    <div class="gallery-description">
      <?= $description; ?>
    </div>
  <?php endif; ?>

  <ul class="gallery list-unstyled">
    <li class="gallery-sizer"></li>
    <?php foreach ($images as $image) : ?>
      <li class="gallery-image thumbnail">
        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
        <?php if($image['caption'] !== "") : ?>
          <div class="caption">
            <p><?= $image['caption']; ?></p>
          </div>
        <?php endif; // end if image caption ?>
      </li>
    <?php endforeach; // end for each ?>
  </ul>
</section>
<?php endif; // end if show gallery and has image
