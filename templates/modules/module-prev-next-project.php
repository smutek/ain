<?php
use Roots\Sage\Extras;
// We need to get the ID of the projects page so we can grab some ACF data.
// 20 is the ID for our projects page. We could hard code it, but if the projects
// page changes (I don't know why it would) this would break until this ID is updated.
// So, there's an option in customizer so that the "page for projects" can be configured
// by the user.
$projectsPage    = get_theme_mod( 'page_for_projects_setting' );
// Get the ACF field from the projects page.
$PostObjects = get_field('select_projects', $projectsPage);
// This will hold ID's of our projects.
$projects = [];
// Grab the ID of the project we are on. Use this to determine where we are in the array.
$currentProject = get_the_ID();
if (is_array($PostObjects) || is_object($PostObjects)) {
  foreach ( $PostObjects as $postObject ) {
    // grab the ID of each project in the repeater.
    $projects[] = $postObject['selected_project']->ID;
  }
}
// set the current post as the index. We'll use this to check if there are items before or after
// in the array. If not we'll return to either the beginning or end, to simulate an endless loop.
$index = array_search($currentProject, $projects);
// If there is another after this, use it as our next post, else use the first post
// Note - use isset() to avoid PHP notices and heartache
isset($projects[$index + 1]) ? $next = $projects[$index + 1] : $next = reset($projects);
// If there is another before this, use it as our prev post, else use the last post
isset($projects[$index -1]) ? $prev = $projects[$index - 1] : $prev = end($projects);

// looping through the output so that we can do this with one chunk of markup.
// Maybe the key value store isn't needed, but I'd rather be sure we're matched up
// correctly.
$displayPosts = [
  'prev' => $prev,
  'next' => $next
];
$classes = [
  'col-xs-6',
  'post-nav'
];

foreach ($displayPosts as $key => $value) :
  // if it's the previous post set class as "prev-post" else set class as "next-post"
  $key === 'prev' ? ($classes[] = 'prev-post') && ($title = 'Previous Project') : ($classes[] = 'next-post') && ($title = 'Next Project');
  // set the post ID
  $postID = $value;
  // set up the data
  //$title    = get_the_title( $postID );
  $excerpt  = Extras\customExcerpt($postID);
  $link     = get_the_permalink( $postID );
  $cover    = get_field( 'project_cover_thumbnail', $postID );
  $background = $cover['url'];
  ?>
  <div class="<?= implode(' ', $classes); ?>" style="background: url(<?= $background; ?>) no-repeat center;">
    <div class="post-nav-inner">
      <a href="<?= $link; ?>">
        <?= $title; ?>
      </a>
    </div>
  </div>

<?php endforeach;
