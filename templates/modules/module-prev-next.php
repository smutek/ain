<?php
use Roots\Sage\Extras;
// get the post type that we are on
$postType = get_post_type();
// holds the ID's for the previous and next posts
$displayPosts = [
  'prev_post' => false,
  'next_post' => false
];
// Checking to see if there is a previous post.
if ( get_adjacent_post( false, '', true ) ) {
  // There is a previous post. Store its ID in our array.
  $previous                  = get_previous_post();
  $displayPosts['prev_post'] = $previous->ID;
}
// Checking to see if there is a next post.
if ( get_adjacent_post( false, '', false ) ) {
  // There is a next post, store the ID
  $next                      = get_next_post();
  $displayPosts['next_post'] = $next->ID;
}
// Layout stuff. Used on outside of card.
$columns = 'equal-height col-sm-6';
// css class array for inside card
$classes = [
  'card'
];
// Set the css classes depending on what type of post we are on
$postType === 'post' ? $classes[] = 'insights-card' : $classes[] = 'news-card';
/*
 * At this point we have an array with 2 key value pairs that contain either
 * a post ID or false. Next do a foreach, gather the necessary info, and output
 * the corresponding markup.
 *
 * Where the value is false we run a query to get either the first or last post.
 */
foreach ( $displayPosts as $key => $value ) :
  // previous
  if ( $key === 'prev_post' ) {
    $classes[] = 'prev-post';
    $order     = 'DESC';
  } // next
  else {
    $classes[] = 'next-post';
    $order     = 'ASC';
  }
  // set the post ID
  $postID = $value;
  // if the value is false (no previous or next post) run a query
  if ( $value === false ) {
    $args  = [
      'post_type'      => $postType,
      'posts_per_page' => 1,
      'order'          => $order
    ];
    $query = new WP_Query( $args );
    $query->the_post();
    $postID = get_the_ID();
    wp_reset_postdata();
  }
  // set up the data
  $title    = get_the_title( $postID );
  $excerpt  = Extras\customExcerpt($postID);
  $link     = get_the_permalink( $postID );
  $thumb    = get_the_post_thumbnail( $postID );
  $authorID = get_post_field( 'post_author', $postID );
  $postDate = get_the_date('', $postID);
  $postsNav = true;
  // output the markup
  ?>
  <div class="<?= $columns; ?>">
    <article <?php post_class( $classes ); ?>>
      <?php
      if ( $thumb ): ?>
        <div class="thumbnail-image featured-image">
          <a href="<?= $link; ?>">
            <?= $thumb; ?>
          </a>
        </div>
      <?php endif; ?>
      <div class="card-content">
        <header class="card-header">
          <h3 class="entry-title"><a href="<?= $link; ?>"><?= $title; ?></a></h3>
          <?php
          // calling the template in this way makes our variables available
          // to the template being called. In this case we are passing the
          // author ID.
          //
          // for alternate implementations see: https://github.com/roots/sage/issues/1414
          include( locate_template( 'templates/entry-meta.php' ) ); ?>
        </header>
        <div class="entry-summary">
          <?= $excerpt; ?>
        </div>
        <footer class="read-more">
          <a href="<?= $link; ?>">Read More</a>
        </footer>
      </div>
    </article>
  </div>
  <?php

endforeach;

