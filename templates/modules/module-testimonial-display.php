<?php
$bgColor = 'bg-white';

// are we in the project post type?
is_singular('projects') ? $isProject = true : $isProject = false;

if ($isProject) {
  // if this is a project set the display option to select, because there
  // is no random option with projects.
  $displayOption = 'select';
  $bgColor = 'bg-' . get_sub_field('testimonial_block_bg_color');
} else {
  // if this is not a project check to see what the display option is.
  $displayOption = get_field( 'testimonial_display_options' );
}
// init some vars
$name = $title = $company = $testimonial = null;

// decide which display option is being called, and get the post data
if ( $displayOption === 'random' ) {
  /*
   * If random, set up a regular query and just grab one post.
   */
  $args = [
    'post_type'      => 'testimonials',
    'posts_per_page' => 1,
    'orderby'        => 'rand'
  ];

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

    $name        = get_the_title();
    $title       = get_field( 'job_title' );
    $company     = get_field( 'company_name' );
    $testimonial = get_the_content();

  endwhile; endif;

} elseif ( $displayOption === 'select' ) {
  /*
   * If display is set to select, it's a post object.
   * Get the data.
   */
  if ($isProject) {
    // if we ended up here from a project we need to get the sub field.
    $post_object = get_sub_field('project_select_testimonial');
  } else {
    // otherwise get the select field
    $post_object = get_field( 'select_testimonial' );
  }
  // now business as usual
  if ( $post_object ):
    // override $post
    $post = $post_object;
    setup_postdata( $post );

    $name        = get_the_title();
    $title       = get_field( 'job_title' );
    $company     = get_field( 'company_name' );
    $testimonial = get_the_content();

  endif; // end post object
}

/*
 * Now we have a name, a title, and a company and we want to output
 * them like: Name, Title, Company
 * but we don't know if any of them have been left blank. So, drop them
 * into an array, run the array through array_filter to remove empty or
 * null items, then through implode to output a comma separated list.
 */
$attributions = [
  $name,
  $title,
  $company
];

$clean  = array_filter( $attributions );
$cite = implode( ', ', $clean );
// finally, output a blockquote, yo
?>

  <section class="testimonial-container <?= $bgColor; ?>">
    <div class="container">
      <figure class="testimonial">
        <blockquote>
          <?= $testimonial; ?>
        </blockquote>
        <footer>
          <cite><?= $cite; ?></cite>
        </footer>
      </figure>
    </div>
  </section>

<?php wp_reset_postdata(); // reset the $post object
