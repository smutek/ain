<?php
// vars
$mp4     = get_field( 'upload_mp4' );
$ogv     = get_field( 'upload_ogg' );
$webm    = get_field( 'upload_webm' );
//
$url     = get_field( 'background_video_url' );
?>
<div class="video-wrap" role="presentation">
  <div class="video-container">
    <video class="video" autoplay="autoplay" loop="loop" muted="">
      <source src="<?= $url; ?>" type="video/mp4">
    </video>
  </div>
</div>
