<?php
use Roots\Sage\Extras;

// vars
$id             = get_the_ID();
$header         = get_field( 'home_hero_heading' );
$bgImage        = get_field( 'hero_background_image' );
$showVideoBG    = get_field( 'show_video_background' );
$probablyPhone  = Extras\probablyAPhone();
$showVideoPopUp = get_field( 'show_pop_up_video' );
$modalId        = "video-modal-" . $id;

// output logo and categories if this is a project page
// client info.

$projectMeta = null;
$isProject = false;

if (is_singular('projects')) {
  $projectMeta = Extras\projectMetaDisplay();
  $isProject = true;
}

?>
<div class="module module-video-hero" style="background: url(<?= $bgImage; ?>) no-repeat;">
  <?php if ( $showVideoBG && ! $probablyPhone ): ?>
    <?php get_template_part( 'templates/modules/module', 'video-background' ); ?>
  <?php endif; ?>
  <div class="container">

    <?php if ( $isProject ) : ?>
      <div class="client-info" role="contentinfo">
        <?php if ( $projectMeta['has_logo'] ) : ?>
          <?= $projectMeta['client_logo']; ?>
        <?php endif; // end if has logo ?>
        <p class="sr-only">Client: <?= $projectMeta['client_name']; ?></p>
      </div>
    <?php endif; // end if is project ?>

    <h2><?= $header; ?></h2>

    <?php if ( $showVideoPopUp ): ?>
      <a class="play-btn" href="#" data-toggle="modal" data-target="#<?= $modalId; ?>">
        <i class="fa fa-play fa-2x fa-inverse" aria-hidden="true"></i>
        <span class="sr-only">Play Video</span>
      </a>
    <?php endif; ?>

    <?php if($isProject) : ?>
      <div class="project-type">
        <?= $projectMeta['project_terms']; ?>
      </div>
    <?php endif; ?>

  </div>
</div>
