<?php
use Roots\Sage\Titles;

/*
 * We have pages in the design whose displayed title does not match the real title.
 * For example, the About page displays "Company and Capabilities" as the title, but
 * the page is called About according to the menu.
 *
 * In these cases the secondary title is added via an ACF over ride consisting of a
 * check box and a text field. When over ride returns true (checked) we keep the real
 * title in an H1 but wrap it in a span with sr-only, so it is still visible to screen
 * readers and search engines, then we output the alternate text, also inside the H1 but
 * inside a small tag with a class of "H1" so it can be styled correctly.
 *
 * Tags inside heading tags are valid, and specifically a small tag is used to denote title
 * text that is less important.
 * @see http://webmasters.stackexchange.com/questions/34195/seo-impact-of-using-small-tag-in-h1
 * This way the document is semantically correct but looks the way the designer intended.
 *
 * This approach may need further discussion as the titles may become too long.
 *
 */

// returns bool - true false
$override = get_field( 'override_title_on' );
// if doing override display title = custom field, else display title = the real title
$override ? $displayTitle = get_field( 'title_override' ) : $displayTitle = Titles\title();
?>
<div class="page-header">
  <?php if ( $override ) : ?>
    <h1>
      <span class="sr-only"><?= Titles\title(); ?>: </span>
      <small class="h1"><?= $displayTitle; ?></small>
    </h1>
  <?php else: ?>
    <h1><?= $displayTitle; ?></h1>
  <?php endif; ?>
</div>




