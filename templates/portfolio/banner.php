<?php
use Roots\Sage\Extras;
// video block
$image = get_sub_field('banner_block_thumbnail_image');
?>

<section class="project-block banner-block">
  <div class="container">
    <div class="banner-image">
      <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
    </div>
  </div>
</section>
