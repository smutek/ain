<?php
use Roots\Sage\Extras;
$bgColor = 'bg-' . get_sub_field('img_text_block_bg_color');
$layout = get_sub_field('img_text_block_image_position');
$classes = $bgColor . ' ' . $layout;
$header = get_sub_field('img_text_block_heading');
$copy = get_sub_field('img_text_block_copy');
$imageField = get_sub_field('img_text_block_img');
$image = Extras\ar_responsive_image($imageField['ID'], 'project_medium', '450px');
?>
<section class="project-block img-text-block <?= $classes; ?>">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 image-container">
        <div class="thumbnail-image">
          <?= $image; ?>
        </div>
      </div>
      <div class="col-sm-8 content-container">
        <div class="block-content">
          <h2><?= $header; ?></h2>
          <?= $copy; ?>
        </div>
      </div>
    </div>
  </div>
</section>

