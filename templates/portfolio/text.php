<?php
//  text_only_block
$bgColor = 'bg-' . get_sub_field('text_only_block_bg_color');
$header = get_sub_field('text_only_block_heading');
$copy = get_sub_field('text_only_block_copy');
?>

<section class="project-block text-block <?= $bgColor; ?>">
  <div class="container">
    <div class="block-content">
      <h2><?= $header; ?></h2>
      <?= $copy; ?>
    </div>
  </div>
</section>
