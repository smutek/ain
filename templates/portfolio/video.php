<?php
use Roots\Sage\Extras;
// video block
$title = get_sub_field('video_block_video_title');
$video = Extras\videoLink('video_block_video_link', true);
$image = get_sub_field('video_block_thumbnail_image');
?>

<section class="project-block video-block">
  <div class="container">
    <div class="project-video">
      <?= $video; ?>
    </div>
  </div>
</section>
